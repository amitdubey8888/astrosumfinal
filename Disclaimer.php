<!doctype html>
<html lang="en">

<!-- Mirrored from astro360horoscope.com/Disclaimer.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Jul 2017 11:32:40 GMT -->
<head>
<meta charset="UTF-8">
<meta name="description" content="Responsive Bootstrap App Landing Page Template">
<meta name="keywords" content="Kane, Bootstrap, Landing page, Template, App, Mobile">
<meta name="author" content="Mizanur Rahman">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>Astrosum - Find Out Your Life.</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/logo.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<link rel="stylesheet" href="assets/app-icons/styles.css">

<!-- WEB FONTS -->
<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">

<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- ANIMATIONS -->
<link rel="stylesheet" href="css/animate.min.css">

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
<link rel="stylesheet" href="css/colors/red.css"> 
<link rel="stylesheet" href="css/colors/green.css"> 
<link rel="stylesheet" href="css/colors/purple.css"> 
<link rel="stylesheet" href="css/colors/orange.css"> 
<link rel="stylesheet" href="css/colors/blue-munsell.css"> 
<link rel="stylesheet" href="css/colors/slate.css"> 
<link rel="stylesheet" href="css/colors/yellow.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">
<!-- JQUERY -->
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>

<body class="container"><p align="justify">The use of our information, products and services should be based on your own due diligence and you agree that our company is not liable for any success or failure of your business that is directly or indirectly related to the purchase and use of our information, products and services.</p>
<p align="justify">Revisions to This Policy: Our company reserves the right to revise, amend, or modify this policy, our Terms of Service agreement, and our other policies and agreements at any time and in any manner, by updating this posting.</p>
<p align="justify">NO EARNINGS PROJECTIONS, PROMISES OR REPRESENTATIONS</p>
<p align="justify">You recognize and agree that we have made no implications, warranties, promises, suggestions, projections, representations or guarantees whatsoever to you about future prospects or earnings, or that you will earn any money, with respect to your referring of Astro 360 App, and that we have not authorized any such projection, promise, or representation by others.</p>
<p align="justify">Any earnings or income statements, or any earnings or income examples, are only estimates of what we think you could earn. There is no assurance you will do as well as stated in any examples. If you rely upon any figures provided, you must accept the entire risk of not doing as well as the information provided. This applies whether the earnings or income examples are monetary in nature or pertain to advertising credits which may be earned (whether such credits are convertible to cash or not).</p>
<p align="justify">There is no assurance that any prior successes or past results as to earnings or income (whether monetary or advertising credits, whether convertible to cash or not) will apply, nor can any prior successes be used, as an indication of your future success or results from any of the information, content, or strategies. Any and all claims or representations as to income or earnings (whether monetary or advertising credits, whether convertible to cash or not) are not to be considered as "average earnings".</p>
<p align="justify"><strong>ALL PAYMENTS MADE WILL BE SUBJECT OF TAX DEDUCTION AT SOURCE AS PER APPLICABLE LAW</strong></p>
<p align="justify"><strong>10% OF THE AMOUNT PAYABLE SHALL BE RETAINED AS DEPOSIT AND WILL BE PAID AT THE END OF FINANCIAL YEAR</strong></p>
<p align="justify"><strong>ALL QUESTIONS ASKED ON ASTRO SUM APP WILL BE SUBJECT OF WAITING PERIOD OF 48 WORKING HOURS (WORKING DAYS HOURS)</strong></p>
<p align="justify"><strong>SUNDAYS, PUBLIC HOLIDAYS AND GOVERNMENT DECLAIRED HOLIDAYS WILL NOT BE COUNTED AS WORKING DAY.</strong></p></body>
</html>