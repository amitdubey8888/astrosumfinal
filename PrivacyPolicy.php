<!doctype html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="description" content="Responsive Bootstrap App Landing Page Template">
<meta name="keywords" content="Kane, Bootstrap, Landing page, Template, App, Mobile">
<meta name="author" content="Mizanur Rahman">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>Astrosum - Find Out Your Life.</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/logo.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<link rel="stylesheet" href="assets/app-icons/styles.css">

<!-- WEB FONTS -->
<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">

<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- ANIMATIONS -->
<link rel="stylesheet" href="css/animate.min.css">

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">

<!-- JQUERY -->
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>

<body class="container">

<p align="Justify">Your privacy is important to us and is committed to protect it in all respect with a view to offer most enriching and holistic internet experience to its users. Please take a moment to familiarize yourself with our privacy practices.</p>

<p align="Justify">1.User Consent</p>

<p align="Justify">By submitting Personal Data through our Website, Application or Offerings, you agree to the terms of this Privacy Policy and you expressly consent to the collection, use and disclosure of your Personal Data in accordance with this Privacy Policy.
To avail services on our application users are required to provide certain information for the registration process that may include to A) Your name B) Address C) email  D) Contact No.</p>

<p align="Justify">2.The Use of Personal Data</p>

<p align="Justify">Data of a personal nature (forename, gender, date and place of birth, location …etc.) about you, collected by Free Horoscope, is processed with the purpose of:</p>
a- personalizing the home page of the website Free Horoscope
b- offering you a personalized service (your horoscope of the day, week, month, etc.).

<p align="Justify">Your email address is retained and stored with your profile, in order to make it secure and to preserve its individuality, but also with a view to enable you to receive, individually, if you request this, your horoscope or special offers by email.
All the required information is service dependent and we may use the above said user information to maintain, protect and improve its services and for developing new services.</p>

<p align="Justify">3.What do we use your information for? </p>

<p align="Justify">Any of the information we collect from you may be used in one of the following ways:</p> 
<ul align="justify">
<li> To personalize your experience
(your information helps us to better respond to your individual needs)</li>

<li>To improve our website
(we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>

<li>To improve customer service
(your information helps us to more effectively respond to your customer service requests and support needs)</li>

</ul>


<p align="Justify">4.Types of data we collect</p>

<p align="Justify">We collect Personal Data from you as described below when you visit our Website, when you send us information or communications, and/or when you use our Application(s) and Offerings. "Personal Data" means data that allows someone to identify or contact you, including, for example, your name, address, telephone number, e-mail address, as well as any other non-public information about you that is associated with or linked to any of the foregoing data.</p>

<p align="Justify">A. Personal Data That You Provide to Us.   In order to provide you with personalized services and features we ask you to register and establish your personal profile ("Profile"). During the | registration process we will ask you for Personal Data such as your name, telephone number, gender, birth information, e-mail address, password, place of residence, zip code, etc. This Personal Data is used to customize the Offerings to you and to provide you with personalized services. Depending on the Offerings you are accessing, you may be asked at various times, for example, by filling out a form or survey, to provide Personal Data such as name, gender, e-mail address, birth date, and birth location. Personal Data is collected primarily to make it easier and more rewarding for you to use our Offerings.</p>

<p align="Justify">B. Advertising.   If there is advertising on the Company Properties, such advertising may employ the use of Cookies or other methods to track hits and click through. We are not responsible for advertiser Cookies or how the information gathered through their use might be used.</p>


<p align="Justify">C. Children.   We do not intentionally gather Personal Data from users who are under the age of 13. If a child under 13 submits Personal Data to Company and we learn that the Personal Data is the information of a child under 13, we will attempt to delete the information as soon as possible. If you believe that we might have any Personal Data from a child under 13.</p>

<p align="Justify">5.Use of your data</p>

<p align="Justify">A.General.   In general, Personal Data you submit to us is used either to respond to requests that you make, or to aid us in serving you better. We use your Personal Data to personalize our Offerings; to deliver personalized information to you; to send you administrative e-mail notifications, such as security or support and maintenance advisories; to provide you with electronic newsletters; and to send you special offers related to our Offerings and for other marketing purposes of Company or our third party partners, should you opt-in to receive such communications by indicating such preference in your Profile. Your e-mail address together with a password is used to secure your Profile and to make our personalized e-mail services available to you.</p>

<p align="Justify">B.Creation and Use of Anonymous and Aggregated Data.   We may create Anonymous Data and Aggregated Data from Personal Data by excluding information (such as your name) that makes the data personally identifiable to you. We use this Anonymous Data and Aggregated Data for trend analysis, and to better understand patterns of usage so that we may enhance the content of our Offerings and improve Website navigation. We reserve the right to use and disclose Anonymous Data and Aggregated Data to third party companies in our discretion. For example, demographic and profile data is shared with our advertisers only in the form of Aggregated Data. Aggregated Data helps our advertisers to tailor their services to the collective characteristics of users of the Company Properties.</p>

<p align="Justify">C.IP Addresses.   We use your IP Address to help diagnose problems with our server, to administer the Website, and to track trends and statistics.</p>

 <p align="Justify">D.Feedback.   If you provide feedback on any of our Company Properties to us, we may use such feedback for any purpose, provided we will not associate such feedback with your Personal Data. We will collect any information contained in such communication and will treat the Personal Data in such communication in accordance with this Privacy Policy.</p>

<p align="Justify">7.Security</p>

<p align="Justify">We have implemented and follow reasonable industry standard technical and procedural measures to protect against unauthorized access and use of your personal information. However, you should know that neither we nor any other website can fully eliminate these risks.</p>

<p align="Justify">8.Changes to our Privacy Policy</p>

<p align="Justify">We have all the rights to make changes in our flow, process, and privacy policy or anything. Even we have all rights to cancel your earnings any time.  There is no legal action accepted.</p>

<p align="Justify">9.Providing the Services You Request</p>

<p align="Justify">We use the information we gather about you to enable your use of our products and services. We may use your email address to notify you of products or services we provide, updates to the Website or the Services, or for customer service and support purposes and any other activity we should need.</p>


<p align="Justify">10.Your Responsibility</p>

<p align="Justify">you are responsible for the security of your account numbers and/or passwords. Make sure you keep them in a safe place and do not share them with others.</p>

<p align="Justify">Always remember to log out after your session ends, to ensure that others cannot access your private personal information. You should take this precaution even if you are not using a public computer, such as at a library or internet café, but even when using your private computer in your home.</p>

<p align="Justify">11.Account and Service-Related Email</p>

<p align="Justify">We reserve the right to send you emails relating to your account status. This includes confirmations, notices of problems, other transactional emails and notifications about changes to the Website or the Services.</p>

<p align="Justify">12.Links to the third party sites/ AD-servers</p>

<p align="Justify">The application may include links to other application such application are governed by their respective privacy policies. Which are beyond our control. Use of any information you provide is governed by privacy policy of the operator of the application, you are visiting. That policy may differ from ours. If you can't find the privacy policy of any of these application via a link from the application homepage, you should contact the application owners directly for more information. When we present information to our advertisement to help them understand our audience and confirm the value of advertising statistics or traffic to various pages/content within our application.</p>
 
<p align="Justify">13.Emails from You</p>
<p align="Justify">If you send us emails, you should be aware that information disclosed in emails may not be secure or encrypted and thus may be available to others. We suggest that you exercise caution when deciding to disclose any personal or confidential information in emails. We will use your email address to respond directly to your questions or comments.</p>

<p align="Justify">14.Statistical Analysis</p>

<p align="Justify">We perform statistical, demographic and marketing analyses of users of the Services, and their subscribing patterns, for product development purposes and to generally inform advertisers about the nature of our subscriber base. We use this information for analysis purposes, including analysis to improve customer relationships.</p>

<p align="Justify">15.Contact Us</p>

<p align="Justify">If you have any questions about this Privacy Policy, the practices of the Company Properties, or your dealings with the Company Properties, you can contact us using the following contact information:</p>


  <address>  
    Gravity E-Com Business Solution Pvt. Ltd.
    SF-14 JTM Building Jagatpura
    Jaipur (Raj.) : 302017
    Email: info@astro360horoscope.com
  </address>

</body>
</html>