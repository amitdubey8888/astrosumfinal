<?php header("Access-Control-Allow-Origin: *"); ?>
<?php defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Astrosumapp extends REST_Controller{

public function __construct() {
parent::__construct();
$this->load->model('Astrosum_numberlogy_model','numberlogymodel');
$this->load->model('PastLife_model','pastlifemodel');

}





public function login_post()
{

$email=$this->input->post('email'); 
$pass=$this->input->post('pass');  
$allval=$this->numberlogymodel->getuserlogin($email, $pass);
if($allval)
{
 $this->set_response($allval, REST_Controller::HTTP_OK); 

}
else 
{
 $this->set_response([["error"=>"Don't have value","message"=>"sorry don't have value "]], REST_Controller::HTTP_NOT_FOUND);
 
}

}



public function updatepro_post()
{

$name=$this->input->post('name'); 
$email=$this->input->post('email');
$mob=$this->input->post('mob');
$dob=$this->input->post('dob');
$place=$this->input->post('place');
  
$allval=$this->numberlogymodel->updatepro($name,$email, $mob,$dob, $place);
if($allval)
{
 $this->set_response($allval, REST_Controller::HTTP_OK); 

}
else 
{
 $this->set_response([["error"=>"Don't have value","message"=>"sorry don't have value "]], REST_Controller::HTTP_NOT_FOUND);
 
}

}





public function getallsaved_get()
{
 $email=$this->input->get('email');
$allval=$this->numberlogymodel->getsaved($email);
if($allval)
{
 $this->set_response($allval, REST_Controller::HTTP_OK); 

}
else 
{
 $this->set_response([["error"=>"Don't have value","message"=>"sorry don't have value "]], REST_Controller::HTTP_NOT_FOUND);
 
}

}






public function setallsaved_post()
{


 $email=$this->input->post('email');
 $name=$this->input->post('name');
$dob=$this->input->post('dob');
$name2=$this->input->post('name2');
$dob2=$this->input->post('dob2');
$time=$this->input->post('time');
$place=$this->input->post('place');


$allval=$this->numberlogymodel->setsaved($email,$name,$dob,$name2,$dob2,$time,$place);
if($allval)
{
 $this->set_response([["status"=>"done","message"=>"success "]], REST_Controller::HTTP_OK); 

}
else 
{
 $this->set_response([["error"=>"Don't have value","message"=>"sorry don't have value "]], REST_Controller::HTTP_NOT_FOUND);
 
}

}







//name numerology 

public function namenumerology_get()
{

$name=$this->input->get('name');

$this->set_response($this->nameNumerology($name), REST_Controller::HTTP_OK);
}


//date numerolorgy 
public function numbernumerology_get()
{

$number=$this->input->get('number');

$this->set_response($this->numberNumerology($number), REST_Controller::HTTP_OK);
}






//****************************past life*****************

public function pastlife_get()
{
$fname=$this->input->get('fname');
$dd=$this->input->get('dd');
$mm=$this->input->get('mm');
$yyyy=$this->input->get('yyyy');
  
if($fname!==NULL && $dd!==NULL && $mm!==NULL && $yyyy!==NULL)
{
$d1=$this->nameNumerology($fname);
$d2=$this->numberNumerology($dd);
$d3=$this->numberNumerology($mm);
$d4=$this->numberNumerology($yyyy);

$data = $this->pastlifemodel->getLife($d1.$d2.$d3.$d4);
$this->set_response($data, REST_Controller::HTTP_OK);
}
else 
{
 $this->set_response([["error"=>"NullPointerException","message"=>"all fields are mandatory to fill" ]], REST_Controller::HTTP_NOT_FOUND);
}


}

//****************************end***********************









//*******************************astro financial chart***************************************** 
public function afc_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
if($name!==NULL && $dob!==NULL)
{

$afc=$this->getafc($name,$dob);

if($afc)
{

$this->set_response([$afc], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"undefineException","message"=>"have logicaly problem" ]], REST_Controller::HTTP_NOT_FOUND);
}
       
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}

//*************************************end ******************************




//*******************************astro financial chart day wise ***************************************** 
public function afcday_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
$month=$this->input->get('month');  
if($name!==NULL && $dob!==NULL && $month!==NULL)
{

$afc=$this->getafcday($name,$dob,$month);

if($afc)
{

$this->set_response($afc, REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"undefineException","message"=>"have logicaly problem" ]], REST_Controller::HTTP_NOT_FOUND);
}
       
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}

//*************************************end ******************************

















//*******************************astro medical chart***************************************** 
public function amc_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
if($name!==NULL && $dob!==NULL)
{

$afc=$this->getafc($name,$dob);

if($afc)
{
for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->getamc($afc['month'.$i], $i);

}

$this->set_response([$afc], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"undefineException","message"=>"have logicaly problem" ]], REST_Controller::HTTP_NOT_FOUND);
}
       
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}

//*************************************end ******************************


//*******************************astro medical chart day wise ***************************************** 
public function amcday_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
$month=$this->input->get('month');  
if($name!==NULL && $dob!==NULL && $month!==NULL)
{

$afc=$this->getafc($name,$dob);

$cd = date('d');
$cm=date('m');
$cy=date('Y');
$cmonthtemp=$cm+0;
$days=array(1,2,9,5,3,6,8);
$dayindex=array("Sunday"=>0,"Monday"=>1,"Tuesday"=>2,"Wednesday"=>3,"Thursday"=>4,"Friday"=>5,"Saturday"=>6);
$cindex=$dayindex[date('l')];


for($z=0;$z<12;$z++)
{
if($cmonthtemp>=13)
{
$cmonthtemp=1;
}

$mlist[$z]=$cmonthtemp;
$cmonthtemp++;

}


//$nod=date(' t ', strtotime($this->intintodate(date('d-m-Y'))));
$nod=cal_days_in_month(CAL_GREGORIAN,$mlist[$month],$cy+0);

$array = array();
if($afc)
{
for($i=0;$i<$nod;$i++)
{
$day=$this->numberNumerology($cd+$cm+$cy);
$cd++;
if($cd>$nod)
{
$cd=1;
$cm=$cm+1;
}
if($cm>=13)
{
$cy=$cy+1;
}
$dz=$this->numberNumerology($afc['month'.($month+1)],$days[$cindex]);
//echo $dz. "    ";
$afcf[$i]=$this->getamcday($dz, $day);
array_push($array , array("day"=>$this->getamcday($dz, $day)));
}

$this->set_response($array, REST_Controller::HTTP_OK);
//$this->set_response($nod, REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"undefineException","message"=>"have logicaly problem" ]], REST_Controller::HTTP_NOT_FOUND);
}
       
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}

//*************************************end ******************************





//******************************* year 360 ***************************************** 
public function year360_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
$type=$this->input->get('type');  
if($name!==NULL && $dob!==NULL  && $type!==NULL)
{

$afc=$this->getafc($name,$dob);

if($afc)
{

if($type==1)
{
for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_family($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}

else if($type==2)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_proparity_vchicle($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==3)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->getamc($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}

else if($type==4)
{


$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==5)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_merredlife($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==6)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_emotional_stability($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==7)
{


for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_respect($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==8)
{


for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_career_education($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==9)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_power_anthority($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else if($type==10)
{

for($i=1;$i<=12;$i++)
{
//$monthvalue=$afc['month'.$a];
$afc['month'.$i]=$this->year_spiritnality_travels($afc['month'.$i], $i);

}
$this->set_response([$afc], REST_Controller::HTTP_OK);


}
else 
{
$this->set_response([["error"=>"typenotmatch","message"=>"type don't match " ]], REST_Controller::HTTP_NOT_FOUND);
}

}
else 
{
$this->set_response([["error"=>"undefineException","message"=>"have logicaly problem" ]], REST_Controller::HTTP_NOT_FOUND);
}
       
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}

//*************************************end year 360******************************










//************************************life 360***********************


public function life360_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  

if($name!==NULL && $dob!==NULL )
{
$nname=$this->nameNumerology($name);
$ndob=$this->numberNumerology($dob);

for($z=1;$z<=9;$z++)
{
$xz=$this->numberlogymodel->getreletionship($nname, $z);
$yz=$this->numberlogymodel->getreletionship($ndob, $z);

$r[$z]=($xz+$yz)/2;
}


$categri=array("Family"=>$this->getavg($r[1],$r[2]),
 "Propertyandvehicle"=>$this->getavg($r[8],$r[9]),
"Health"=>$this->getavg($r[1],$r[9]),
"FinanceandProfits"=>$this->getavg($r[4],$r[5]),
"Merriedlife"=>$this->getavg($r[3],$r[6]),
"EmotionalStability"=>$this->getavg($r[4],$r[7]),
"Respectgainsandstatus"=>$this->getavg($r[3],$r[8]),
"CareerandEducation"=>$this->getavg($r[3],$r[5]),
"PowerandhigherAuthority"=>$this->getavg($r[1],$r[3]),
"Spiritualityandtravels"=>$this->getavg($r[7],$r[8]));


$this->set_response([$categri], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************end life 360********************






//************************************kids career***********************


public function kidcareer_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  

if($name!==NULL && $dob!==NULL )
{
$nname=$this->nameNumerology($name);
$ndob=$this->numberNumerology($dob);

for($z=1;$z<=9;$z++)
{
$xz=$this->numberlogymodel->getreletionship($nname, $z);
$yz=$this->numberlogymodel->getreletionship($ndob, $z);

$r["kid".$z]="".($xz+$yz)/2;
}



$this->set_response([$r], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************end kids career********************





//**************************certainty 360******************

public function certainty_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
$question=$this->input->get('question');  
if($name!==NULL && $dob!==NULL )
{
$nname=$this->nameNumerology($name);
$ndob=$this->numberNumerology($dob);

for($i=0;$i<strlen($question);$i++)
{
$qz[$i]=$this->nameNumerology($question[$i]);

}

//$this->set_response($qz, REST_Controller::HTTP_OK);
$j1=count($qz);
while($j1!=1)
{
$j=0;
while((count($qz)-$j)!=1)
{
 $qz[$j]=$this->numberNumerology($qz[$j]+$qz[$j+1]);
$j++;
}
$j1--;
}

$z=$qz[0];

$xz=$this->numberlogymodel->getreletionship($nname, $z);
$yz=$this->numberlogymodel->getreletionship($ndob, $z);

$avg=($xz+$yz)/2;


$this->set_response([["certainty"=>$avg]], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//************************** end certainty 360******************




//************************************annual pains and gains***********************


public function annualpans_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  
$year=date('Y');
if($name!==NULL && $dob!==NULL )
{
$nname=$this->nameNumerology($name);
$ndob=$this->numberNumerology($dob);
$a=$this->numberNumerology($year);

for($z=1;$z<=9;$z++)
{
$xz=$this->numberlogymodel->getreletionship($nname, $z);
$yz=$this->numberlogymodel->getreletionship($ndob, $z);
$az=$this->numberlogymodel->getreletionship($a, $z);
$r[$z]=($xz+$yz+$az)/3;
}


$categri=array("Family"=>$this->getavg($r[1],$r[2]),
 "Propertyandvehicle"=>$this->getavg($r[8],$r[9]),
"Health"=>$this->getavg($r[1],$r[9]),
"FinanceandProfits"=>$this->getavg($r[4],$r[5]),
"Merriedlife"=>$this->getavg($r[3],$r[6]),
"EmotionalStability"=>$this->getavg($r[4],$r[7]),
"Respectgainsandstatus"=>$this->getavg($r[3],$r[8]),
"CareerandEducation"=>$this->getavg($r[3],$r[5]),
"PowerandhigherAuthority"=>$this->getavg($r[1],$r[3]),
"Spiritualityandtravels"=>$this->getavg($r[7],$r[8]));


$this->set_response([$categri], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************annual pains and gains********************








//************************************vivah sutra ***********************


public function vivahsutra_get()
{
$name1=$this->input->get('mname');
$dob1=$this->input->get('mdob');
$name2=$this->input->get('fname');
$dob2=$this->input->get('fdob');
  

if(($name1!==NULL) && ($dob1!==NULL)  && ($name2!==NULL) && ($dob2!==NULL))
{
$x1=$this->nameNumerology($name1);
$y1=$this->numberNumerology($dob1);
$x2=$this->nameNumerology($name2);
$y2=$this->numberNumerology($dob2);




$categri=array("A"=>(((((
($this->numberlogymodel->getreletionship($x1, 2)+$this->numberlogymodel->getreletionship($x1,4))+
($this->numberlogymodel->getreletionship($y1, 2)+$this->numberlogymodel->getreletionship($y1,4))+
($this->numberlogymodel->getreletionship($x2, 2)+$this->numberlogymodel->getreletionship($x2,4))+
($this->numberlogymodel->getreletionship($y2, 2)+$this->numberlogymodel->getreletionship($y2,4)))*9)/8)*.54)/.50),





 "B"=>(((((
($this->numberlogymodel->getreletionship($x1, 1)+$this->numberlogymodel->getreletionship($x1,5)) +
($this->numberlogymodel->getreletionship($y1, 1)+$this->numberlogymodel->getreletionship($y1,5)) +
($this->numberlogymodel->getreletionship($x2, 1)+$this->numberlogymodel->getreletionship($x2,5)) +
($this->numberlogymodel->getreletionship($y2, 1)+$this->numberlogymodel->getreletionship($y2,5)) )*9)/8)*.55)/.52),

"C"=>(((((
($this->numberlogymodel->getreletionship($x1, 1)+$this->numberlogymodel->getreletionship($x1, 4)) +
($this->numberlogymodel->getreletionship($y1, 1)+$this->numberlogymodel->getreletionship($y1, 4)) +
($this->numberlogymodel->getreletionship($x2, 1)+$this->numberlogymodel->getreletionship($x2, 4)) +
($this->numberlogymodel->getreletionship($y2, 1)+$this->numberlogymodel->getreletionship($y2, 4)) )*9)/8)*.50)/.52),

"D"=>(((((
($this->numberlogymodel->getreletionship($x1,6)+$this->numberlogymodel->getreletionship($x1, 9)) +
($this->numberlogymodel->getreletionship($y1,6)+$this->numberlogymodel->getreletionship($y1, 9)) +
($this->numberlogymodel->getreletionship($x2,6)+$this->numberlogymodel->getreletionship($x2, 9)) +
($this->numberlogymodel->getreletionship($y2,6)+$this->numberlogymodel->getreletionship($y2, 9)) )*9)/8)*.65)/.50),

"E"=>(((((
($this->numberlogymodel->getreletionship($x1, 4)+$this->numberlogymodel->getreletionship($x1,6)) +
($this->numberlogymodel->getreletionship($y1, 4)+$this->numberlogymodel->getreletionship($y1,6)) +
($this->numberlogymodel->getreletionship($x2, 4)+$this->numberlogymodel->getreletionship($x2,6)) +
($this->numberlogymodel->getreletionship($y2, 4)+$this->numberlogymodel->getreletionship($y2,6)) )*9)/8)*.68)/.50),

"F"=>(((((
($this->numberlogymodel->getreletionship($x1,6)+$this->numberlogymodel->getreletionship($x1, 8)) +
($this->numberlogymodel->getreletionship($y1,6)+$this->numberlogymodel->getreletionship($y1, 8)) +
($this->numberlogymodel->getreletionship($x2,6)+$this->numberlogymodel->getreletionship($x2, 8)) +
($this->numberlogymodel->getreletionship($y2,6)+$this->numberlogymodel->getreletionship($y2, 8)) )*9)/8)*.45)/.50),

"G"=>(((((
($this->numberlogymodel->getreletionship($x1, 4)+$this->numberlogymodel->getreletionship($x1,7)) +
($this->numberlogymodel->getreletionship($y1, 4)+$this->numberlogymodel->getreletionship($y1,7)) +
($this->numberlogymodel->getreletionship($x2, 4)+$this->numberlogymodel->getreletionship($x2,7)) +
($this->numberlogymodel->getreletionship($y2, 4)+$this->numberlogymodel->getreletionship($y2,7)) )*9)/8)*.38)/.50),

"H"=>(((((
($this->numberlogymodel->getreletionship($x1, 2)+$this->numberlogymodel->getreletionship($x1, 6)) +
($this->numberlogymodel->getreletionship($y1, 2)+$this->numberlogymodel->getreletionship($y1, 6)) +
($this->numberlogymodel->getreletionship($x2, 2)+$this->numberlogymodel->getreletionship($x2, 6)) +
($this->numberlogymodel->getreletionship($y2, 2)+$this->numberlogymodel->getreletionship($y2, 6)) )*9)/8)*.55)/.50),

"I"=>(((((
($this->numberlogymodel->getreletionship($x1,6)+$this->numberlogymodel->getreletionship($x1, 8)) +
($this->numberlogymodel->getreletionship($y1,6)+$this->numberlogymodel->getreletionship($y1, 8)) +
($this->numberlogymodel->getreletionship($x2,6)+$this->numberlogymodel->getreletionship($x2, 8)) +
($this->numberlogymodel->getreletionship($y2,6)+$this->numberlogymodel->getreletionship($y2, 8)) )*9)/8)*.52)/.40),

"J"=>(((((
($this->numberlogymodel->getreletionship($x1,5)+$this->numberlogymodel->getreletionship($x1, 7)) +
($this->numberlogymodel->getreletionship($y1,5)+$this->numberlogymodel->getreletionship($y1, 7)) +
($this->numberlogymodel->getreletionship($x2,5)+$this->numberlogymodel->getreletionship($x2, 7)) +
($this->numberlogymodel->getreletionship($y2,5)+$this->numberlogymodel->getreletionship($y2, 7)) )*9)/8)*.35)/.40));



$this->set_response([$categri], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************annual pains and gains********************









//************************************partner compatibility ***********************


public function partner_get()
{
$name1=$this->input->get('pname1');
$dob1=$this->input->get('dob1');
$name2=$this->input->get('pname2');
$dob2=$this->input->get('dob2');
  

if(($name1!==NULL) && ($dob1!==NULL)  && ($name2!==NULL) && ($dob2!==NULL))
{
$x1=$this->nameNumerology($name1);
$y1=$this->numberNumerology($dob1);
$x2=$this->nameNumerology($name2);
$y2=$this->numberNumerology($dob2);




$categri=array("A"=>(((((
($this->numberlogymodel->getreletionship($x1, 2)+$this->numberlogymodel->getreletionship($x1,4)) +
($this->numberlogymodel->getreletionship($y1, 2)+$this->numberlogymodel->getreletionship($y1,4)) +
($this->numberlogymodel->getreletionship($x2, 2)+$this->numberlogymodel->getreletionship($x2,4)) +
($this->numberlogymodel->getreletionship($y2, 2)+$this->numberlogymodel->getreletionship($y2,4)) )*9)/8)*.54)/.54),





 "B"=>(((((
($this->numberlogymodel->getreletionship($x1, 1)+$this->numberlogymodel->getreletionship($x1, 9)) +
($this->numberlogymodel->getreletionship($y1, 1)+$this->numberlogymodel->getreletionship($y1, 9)) +
($this->numberlogymodel->getreletionship($x2, 1)+$this->numberlogymodel->getreletionship($x2, 9)) +
($this->numberlogymodel->getreletionship($y2, 1)+$this->numberlogymodel->getreletionship($y2, 9)) )*9)/8)*.52)/.55),

"C"=>(((((
($this->numberlogymodel->getreletionship($x1, 3)+$this->numberlogymodel->getreletionship($x1,5)) +
($this->numberlogymodel->getreletionship($y1, 3)+$this->numberlogymodel->getreletionship($y1,5)) +
($this->numberlogymodel->getreletionship($x2, 3)+$this->numberlogymodel->getreletionship($x2,5)) +
($this->numberlogymodel->getreletionship($y2, 3)+$this->numberlogymodel->getreletionship($y2,5)) )*9)/8)*.52)/.50),

"D"=>(((((
($this->numberlogymodel->getreletionship($x1, 3)+$this->numberlogymodel->getreletionship($x1, 8)) +
($this->numberlogymodel->getreletionship($y1, 3)+$this->numberlogymodel->getreletionship($y1, 8)) +
($this->numberlogymodel->getreletionship($x2, 3)+$this->numberlogymodel->getreletionship($x2, 8)) +
($this->numberlogymodel->getreletionship($y2, 3)+$this->numberlogymodel->getreletionship($y2, 8)) )*9)/8)*.50)/.65),

"E"=>(((((
($this->numberlogymodel->getreletionship($x1, 4)+$this->numberlogymodel->getreletionship($x1,5)) +
($this->numberlogymodel->getreletionship($y1, 4)+$this->numberlogymodel->getreletionship($y1,5)) +
($this->numberlogymodel->getreletionship($x2, 4)+$this->numberlogymodel->getreletionship($x2,5)) +
($this->numberlogymodel->getreletionship($y2, 4)+$this->numberlogymodel->getreletionship($y2,5)) )*9)/8)*.50)/.68),


"F"=>(((((
($this->numberlogymodel->getreletionship($x1, 2)+$this->numberlogymodel->getreletionship($x1,5)) +
($this->numberlogymodel->getreletionship($y1, 2)+$this->numberlogymodel->getreletionship($y1,5)) +
($this->numberlogymodel->getreletionship($x2, 2)+$this->numberlogymodel->getreletionship($x2,5)) +
($this->numberlogymodel->getreletionship($y2, 2)+$this->numberlogymodel->getreletionship($y2,5)) )*9)/8)*.50)/.45),

"G"=>(((((
($this->numberlogymodel->getreletionship($x1,3)+$this->numberlogymodel->getreletionship($x1, 6)) +
($this->numberlogymodel->getreletionship($y1,3)+$this->numberlogymodel->getreletionship($y1, 6)) +
($this->numberlogymodel->getreletionship($x2,3)+$this->numberlogymodel->getreletionship($x2, 6)) +
($this->numberlogymodel->getreletionship($y2,3)+$this->numberlogymodel->getreletionship($y2, 6)) )*9)/8)*.50)/.38),

"H"=>(((((
($this->numberlogymodel->getreletionship($x1,5)+$this->numberlogymodel->getreletionship($x1, 8)) +
($this->numberlogymodel->getreletionship($y1,5)+$this->numberlogymodel->getreletionship($y1, 8)) +
($this->numberlogymodel->getreletionship($x2,5)+$this->numberlogymodel->getreletionship($x2, 8)) +
($this->numberlogymodel->getreletionship($y2,5)+$this->numberlogymodel->getreletionship($y2, 8)) )*9)/8)*.50)/.55),

"I"=>(((((
($this->numberlogymodel->getreletionship($x1, 2)+$this->numberlogymodel->getreletionship($x1, 6)) +
($this->numberlogymodel->getreletionship($y1, 2)+$this->numberlogymodel->getreletionship($y1, 6)) +
($this->numberlogymodel->getreletionship($x2, 2)+$this->numberlogymodel->getreletionship($x2, 6)) +
($this->numberlogymodel->getreletionship($y2, 2)+$this->numberlogymodel->getreletionship($y2, 6)) )*9)/8)*.40)/.52),

"J"=>(((((
($this->numberlogymodel->getreletionship($x1, 4)+$this->numberlogymodel->getreletionship($x1,6)) +
($this->numberlogymodel->getreletionship($y1, 4)+$this->numberlogymodel->getreletionship($y1,6)) +
($this->numberlogymodel->getreletionship($x2, 4)+$this->numberlogymodel->getreletionship($x2,6)) +
($this->numberlogymodel->getreletionship($y2, 4)+$this->numberlogymodel->getreletionship($y2,6)) )*9)/8)*.40)/.35));



$this->set_response([$categri], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************partner end ********************


//************************************my favourites ***********************


public function myfavourites_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  

if($name!==NULL && $dob!==NULL )
{
$nname=$this->nameNumerology($name);
$ndob=$this->numberNumerology($dob);

$planet=array('Sun','Moon','Jupiter','Rahu','Mercury','Venus','Ketu','Saturn','Mars');


for($z=1;$z<=9;$z++)
{
$xz=$this->numberlogymodel->getreletionship($nname, $z);
$yz=$this->numberlogymodel->getreletionship($ndob, $z);

$r[$z]=($xz+$yz)/2;
}

$data = json_encode($r);

$max1 = array_keys($r, max($r));
$planet1 = $planet[$max1[0]-1];

unset($r[$max1[0]]);


$max1 = array_keys($r, max($r));
$planet2 = $planet[$max1[0]-1];

$data = json_encode($r);

//$planet2 = $planet[$maxs[1]];

/*
$largestA = $r[1];
$largestB = 0;
for($i = 1; $i < count($r); $i++){

        if($r[$i] > $largestA){
            $largestB = $largestA;
            $largestA = $planet[$i];
        }
        else if ($r[$i] > $largestB && $r[$i] != $largestA) {
            $largestB = $planet[$i];
        }
}
*/
$qval=$this->numberlogymodel->getmyfav($planet1,$planet2);
if($qval)
{
$this->set_response($qval, REST_Controller::HTTP_OK);
//$this->set_response([["error"=>"$max1","message"=>"$data" ]], REST_Controller::HTTP_NOT_FOUND);

}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"$data" ]], REST_Controller::HTTP_NOT_FOUND);
}



//$fff=array("first1"=>, "sec2"=>  );


//$this->set_response([$fff], REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}

}



//***********************************end my favourites ********************


//************************************Hourly predection  ***********************


public function horapred_get()
{
$name=$this->input->get('name');
$dob=$this->input->get('dob');  



if($name!==NULL && $dob!==NULL )
{


$do=array(
    "Saturn"=>"Executive character and abilities, political skills, expert handling of power and authority, working for a cause, achieving recognition, exercising sound judgment, decisive and commanding. ",
    "Jupiter"=>"Self-expressive in many ways, verbalization, inspiration and keen imagination, artistic gifts, accurate impressions and insights, never-ending optimism, happy and fun-loving, enjoys life fully.",
    "Mars"=>"Heartily friendly and congenial, a hail-fellow, humanitarian instincts, a giving nature, selflessness, obligations, creative expression, readily influenced to do good works, artistic and writing talents.",
    "Sun"=>"Initiator of action, a pioneering spirit, inventive ideas, strong leadership skills, independent, drives to attain, individualistic style, executive abilities, extraordinary will, and determination, courageous.",
    "Venus"=>"A strong sense of responsibility, artistic, a nurturing disposition, community oriented, balanced, sympathy for others, a humanitarian, unselfishness, love of home and domestic affairs, freely renders service to others.",
    "Mercury"=>"Expansiveness, new and visionary ideas, quick thinking, versatile and ever-changing, action oriented, curious and exploring, promoting, resourceful in using freedom constructively. ",
    "Moon"=>"Cooperation, adaptability, considerate of others, sensitive to the needs of others, partnering, an arbiter or mediator, modest, sincere, spiritually influenced, a diplomat.");

$donot=array(
    "Saturn"=>"Workaholic, overly ambitious, lacking humanitarian instincts, mismanaging money, repressing subordinates, impatient with people, stressed, materialistic.",
    "Jupiter"=>"Scattered energies, exaggeration, unfinished projects, lack of direction, moodiness, self-centeredness.",
    "Mars"=>"Self-adulation, scattered interests, possessiveness, moodiness, careless with finances, wanting peer attention. ",
    "Sun"=>"Overly assertive or aggressive, dominating, impulsiveness, egotistic, boastfulness, willfulness.",
    "Venus"=>"Self-righteousness, obstinacy, stubborn, dominates family and friends, meddling, egotistical and susceptible to flattery, outspoken. ",
    "Mercury"=>"Restless, discontent, edgy temperament and speech, dissatisfaction, too many hasty decisions, impatience, lacking in application. ",
    "Moon"=>"Shyness, timidity, fear, self-consciousness, drown in detail, depression");




 $tz=array(
    "Saturn"=>8,
    "Jupiter"=>3,
    "Mars"=>9,
    "Sun"=>1,
    "Venus"=>6,
    "Mercury"=>5,
    "Moon"=>2);
 $data = file_get_contents('http://astrosum.com/astrosum_api/astroapi/src/hora.php');
 $alldata = json_decode($data, TRUE);
 $hora=$alldata["hora"];
 $day=$hora["day"];
 $night=$hora["night"];

 for($i=0;$i<count($day);$i++)
 {
 $dayindex=$day[$i];
 $daytime[$i]=$dayindex["time"];
 $dayhora[$i]=$dayindex["hora"];
 $z[$i]=$tz[$dayhora[$i]];
 }


for($i=0;$i<count($night);$i++)
 {
 $nightindex=$night[$i];
 $nighttime[$i]=$nightindex["time"];
 $nighthora[$i]=$nightindex["hora"];
 $nz[$i]=$tz[$nighthora[$i]];
 }



 $nname=$this->nameNumerology($name);
 $ndob=$this->numberNumerology($dob);


 for($i=0;$i<count($z);$i++)
 {
 $xz=$this->numberlogymodel->getreletionship($nname, $z[$i]);
 $yz=$this->numberlogymodel->getreletionship($ndob, $z[$i]);
 $avg[$i]=($xz+$yz)/2;
 }

for($i=0;$i<count($nz);$i++)
 {
 $nxz=$this->numberlogymodel->getreletionship($nname, $nz[$i]);
 $nyz=$this->numberlogymodel->getreletionship($ndob, $nz[$i]);
 $avgn[$i]=($nxz+$nyz)/2;
 }




$j=0;
 for($i=0;$i<24;$i++)
 {
if($i<12)
{
$nh=$nighthora[$i];
 $fa[$i]=array("hora"=>$avg[$i], "time"=>$daytime[$i], "do"=>$do[$nh], "donot"=>$donot[$nh]);
}
else 
{
$dh=$dayhora[$j];
 $fa[$i]=array("hora"=>$avgn[$j], "time"=>$nighttime[$j], "do"=>$do[$dh], "donot"=>$donot[$dh]);
$j++;
}
 }





$this->set_response($fa, REST_Controller::HTTP_OK);
}
else 
{
$this->set_response([["error"=>"NullPointerException","message"=>"name and dob not null" ]], REST_Controller::HTTP_NOT_FOUND);
}


}



//***********************************end Hourly predection  ********************








}
