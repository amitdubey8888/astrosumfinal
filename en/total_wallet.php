<?php

require 'database.php';
$user_id=$_REQUEST['user_id'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$timestamp=$db->getCurrentDateTime();
	if ($result = $mysqli->query("SELECT `total_points` FROM `user` WHERE `id`='$user_id'")) 
	{
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message="You don't have enough astro points to submit this query. please goto 'Add Credit' menu and earn astro points.";	
		}
		else
		{
			$row=$result->fetch_array(MYSQLI_NUM);
			$total_earned_points=$row[0];
			$error=0;
			$message=$total_earned_points;
		}
		/* close result set */
		$result->close();
	}
	else
	{
		$error=1;
		$message='Astro points record not found.';
	}
	/* close connection */
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	echo json_encode($response_arr);
}

?>