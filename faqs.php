<!doctype html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="description" content="Responsive Bootstrap App Landing Page Template">
<meta name="keywords" content="Kane, Bootstrap, Landing page, Template, App, Mobile">
<meta name="author" content="Mizanur Rahman">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>Astrosum - Find Out Your Life.</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/logo.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<link rel="stylesheet" href="assets/app-icons/styles.css">

<!-- WEB FONTS -->
<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">

<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- ANIMATIONS -->
<link rel="stylesheet" href="css/animate.min.css">

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
<link rel="stylesheet" href="css/colors/red.css"> 
<link rel="stylesheet" href="css/colors/green.css"> 
<link rel="stylesheet" href="css/colors/purple.css"> 
<link rel="stylesheet" href="css/colors/orange.css"> 
<link rel="stylesheet" href="css/colors/blue-munsell.css"> 
<link rel="stylesheet" href="css/colors/slate.css"> 
<link rel="stylesheet" href="css/colors/yellow.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">
<!-- JQUERY -->
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</head>

<body class="container"><ol align="justify">
<li><strong>Why should I believe in astrology?</strong></li>
</ol>
<p align="justify"><strong>Ans.</strong>&nbsp;Astrology helps you on two planes. On material plane, it lets you understand your strength and weakness. It helps you to decide whom to trust and whom not to. It guides you through the right times to conduct right activities. On spiritual plane, it helps you to understand your mission in this life. You can reach the ultimate goal of self-realization through Astrology. If you do not believe in Astrology, then you cannot get these benefits of Astrology which believers get.</p>
<ol start="2" align="justify">
<li><strong>What is Astrosum?</strong></li>
</ol>
<p align="justify"><strong>Ans.</strong>&nbsp;Astrosum includes 15 categories which are :</p>
<ul align="justify">
<li>Astro medical chart</li>
<li>Astro financial chart</li>
<li>Question 360 yes/no</li>
<li>Vivah sutra predictions</li>
<li>Certainty predictions</li>
<li>Love matrix</li>
<li>Career</li>
<li>Foreign travel</li>
<li>Matrimony</li>
<li>Business/ partner</li>
<li>Credit/debit</li>
<li>Life predictions</li>
<li>Good news</li>
<li>Precautions</li>
<li>Year 360</li>
</ul>
<ol start="3" align="justify">
<li><strong>What should I do for asking question?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>Points are defined there for each category so for asking question related with specific category you have to have defined points in your wallet. To add points in your wallet you have to go on&nbsp;<strong>Add credit&nbsp;</strong>option and then download some app from there and then after points will be credited to your wallet. Yeah now you are able to ask question.</p>
<ol start="4" align="justify">
<li><strong>I have installed an app but did not receive credit in my account?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>We are sorry that you have not earned credit for installing apps. It will take 15 to 60 minutes to credit points in your wallet. Do not uninstall the app during this period.</p>
<ol start="5" align="justify">
<li><strong>Why is my birth time important?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>The planets are constantly moving around and switching from aspect to aspect, so the difference of an hour can affect your birth chart, Moon sign and Rising sign &ndash; and in turn, this will affect who you are. You can find your birth time information from your birth certificate or by asking your parents. The closer you can get your specific birth time, the better. Then you can look up your Essential Birth Chart Report and start exploring yourself further.</p>
<ol start="6" align="justify">
<li><strong>But astrology is fortune-telling, and the future is scary. Some of that stuff I don&rsquo;t want to know!!</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>Of course you don&rsquo;t want to hear your &ldquo;fate,&rdquo; because your future&rsquo;s not carved in stone. There is no &ldquo;fate.&rdquo;&nbsp;<strong><em>You</em></strong>&nbsp;create your future. But your life does have a certain shape and&nbsp;<strong><em>knowing</em></strong>&nbsp;that shape can put you back in the driver&rsquo;s seat. You are unique and the future you&rsquo;ll create is different from anyone else&rsquo;s. Its trends can be predicted by a good astrologer. In an astrology reading you can find out:</p>
<ul align="justify">
<li>What the changes you&rsquo;re going through are</li>
<li>Why things are happening to you right now</li>
<li>How it feels and what to do about those feelings</li>
<li>How the timing of it will unfold</li>
<li>How to handle it all, and</li>
<li>When it will be over</li>
</ul>
<ol start="7" align="justify">
<li><strong>Why is an accurate time important?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>The birth chart shifts subtly every three minutes. A ten minutes&rsquo; difference between two charts can spell a lot of personality change. This is part of why even identical twins are not always temperamentally identical. If your chart is off by as little as ten minutes, it can throw predictions of the timing of your future experiences off by years&mdash;and the discrepancy gets worse as you age.</p>
<ol start="8" align="justify">
<li><strong>What is AMC (Astro Medical Chart)?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;M</strong>edical Astrology is the branch of astrology that deals with the workings of the human body.&nbsp; A competent medical astrologer can analyze a person's birth chart and determine bodily strengths and weaknesses, proneness to various disease states, and nutritional deficiencies. In the event of&nbsp;illness or disease, a medical astrologer will use predictive methods to try to determine the severity and duration of the disease.&nbsp;Sometimes a medical astrologer can help determine the course of a disease by the use of a chart called a discomfiture chart.&nbsp;The medical astrologer can pick the best date(s) and time(s) for an elective procedure anything from a face lift to a college injection.&nbsp;&nbsp;&nbsp;Although it is always best to have the time of&nbsp;birth of the person, if this is not possible, the medical astrologer can&nbsp;still help the client pick the most fortunate&nbsp;times for the procedure.&nbsp;Needless to say, one should not try to use elective astrology in a medical emergency.</p>
<ol start="9" align="justify">
<li><strong>What is AFC (Astro Financial Chart)?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;&nbsp;</strong>Some people use fundamental analysis of the companies to invest in their shares. Other people use technical analysis to understand the behavior of the masses towards the stock prices. Both these methods have their advantages, yet they&nbsp;fail to see the uncertainties of the future events. We believe astrological analysis of&nbsp;a stock-chart can provide useful insights&nbsp;about the future performance of a stock,&nbsp;and should&nbsp;be used in conjunction with the fundamental and technical analysis. We will provide you the all financial benefits schemes according to your planetary positions.</p>
<ol start="10" align="justify">
<li><strong>What is Certainty predictions?</strong></li>
</ol>
<p align="justify"><strong>Ans.&nbsp;</strong>Most astrologers use a variety of techniques to "look into the future" of an individual. There are plenty of techniques used to make predictions for&mdash;and plan for&mdash;the future. Here, we share our preferred techniques&mdash;transits,&nbsp;secondary progressions,&nbsp;solar arc directions,&nbsp;solar return charts, and more. Transits, for example, challenge and pressure us to make changes in our lives and in our attitudes toward our lives. They bring us joy and sometimes sorrow&mdash;they pull out parts of ourselves and "force" us to face them. Secondary progressions reveal our cycles of maturity and growth, showing us how our personalities unfold.&nbsp;Outer planet transits affect us in large chunks&mdash;they can be active before the transit is exact by aspect, but we have seen the following: Many times an outer planet transits our natal point once by direct motion, moves forward and then retrogrades back over that point, only to turn direct again and pass that same natal point for the final time in that cycle.&nbsp;This technique will provide you the best certainty predictions.</p>
<p align="justify">&nbsp;</p></body>
</html>