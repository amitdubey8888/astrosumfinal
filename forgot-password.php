<?php header("Access-Control-Allow-Origin: *"); ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Astrosum - Find Out Your Life.</title>
<meta name="description" content="Astro 360 is a Free Android application for your daily life Solutions based on Astrology calculations, you can submit question about your problems and get the answer in 24Hours">
<meta name="keywords" content="Astrology, Horoscope, 360, Free android application">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/logo.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">
<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- FONT ICONS -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<link rel="stylesheet" href="assets/app-icons/styles.css">
<!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
<!-- WEB FONTS -->
<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">
<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">
<!-- ANIMATIONS -->
<link rel="stylesheet" href="css/animate.min.css">
<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">
<!-- COLORS -->
<link rel="stylesheet" href="css/blue.css">
<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/main-style.css">
<!-- JQUERY -->
<script src="js/jquery.min.js"></script>
<style>
 body{
   background:aliceblue;
 }
 #forgot-pass-heading{
    margin-top:30px;
    font-family: -webkit-body;
    font-weight: 600;
    font-size: xx-large;
 }
 #reset-email{
   margin-top:10px;
   font-family: -webkit-body;
 }
 input[type=text]{
   background:aliceblue;
 }
.form-control-input{
    display: block;
    width: 32% !important;
    height: 50px;
    margin: auto;
    padding: 6px 12px;
    font-size: 15px;
    line-height: 1.428571429;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
}
@media only screen and (max-width:1125px){
  .form-control-input{
    width:39% !important;
  }
}
@media only screen and (max-width:480px){
  .form-control-input{
    width:65% !important;
  }
  input[type=submit]{
    width: 40% !important;
  }
  .forgot-pass{
    margin-top: 15px !important;
    padding: 25px;
  }
}
input[type=submit]{
  height: 50px;
  width: 10%;
  margin-top: 10px;
  font-size: larger;
  font-family: -webkit-body;
}
.forgot-pass{
  margin-top: 25px;
  padding: 25px;
}
</style>
</head>
<body>
<div class="forgot-pass">
  <div class="row">
    <h4 id="forgot-pass-heading" class="text-center">Forgot Password!</h4>
    <form action="forgot-password.php" method="post">
      <p id="reset-email" class="text-center">Please enter you email, We will send your password to registered email Id.</p>
      <h5 id="reset-link" style="text-align:center;color:red; display:none;"><i>Your password has been sent to your email, Don't forgot to check spam folder!</i></h5>
      <h5 id="wrong-link" style="text-align:center;color:red; display:none;">An unwanted error occured, Please Try Again.</h5>      
      <input type="text" name="email" class="form-control-input" placeholder="Enter Email Here">
      <small id="invalid-email" style="text-align:center;color:red;display:none;">Invalid Email Format, Please Enter An Valid Email Address.</small>
      <small id="no-account" style="text-align:center;color:red;display:none;">We can't find any account with this email, Please remember your email.</small>      
      <input type="submit" class="btn btn-success" value="Submit" name="reset_password">
    </form>
  </div>
</div>

<footer id="contactus" style="margin-top: 15px;">
<div class="container">
	<!-- LOGO -->
	<img src="images/logo.png" width="200" height="200" alt="LOGO" class="responsive-img">
	<!-- SOCIAL ICONS -->
    <ul class="social-icons">
		<li><a href="https://www.facebook.com/gravitydailyhoroscope/?fref=ts"><i class="social_facebook_square" style="color: blue;"></i></a></li>
		<li><a href="https://twitter.com/Astroapp360"><i class="social_twitter_square" style="color: blue;"></i></a></li>
		<li><a href="http://in.pinterest.com/AstrosumApp"><i class="social_pinterest_square" style="color: blue;"></i></a></li>
		<li><a href="http://plus.google.com/109689866191177132746"><i class="social_googleplus_square" style="color: blue;"></i></a></li>
	</ul>	
	<!-- COPYRIGHT TEXT -->
	<font size="2px"><table width="100%"><tbody><tr><td align="left"> <a href="aboutus.php" style="color: #737373">About Us</a> | <a href="TermsService.php" style="color:#737373"> Terms of Service
  </a> | <a href="PrivacyPolicy.php" style="color: #737373">Privacy Policy</a> | <a href="Disclaimer.php" style="color: #737373">Disclaimer</a> | <a href="faqs.php" style="color: #737373">FAQs</a></td>
	<!-- COPYRIGHT TEXT -->
	<td align="left">
		©2017 Gravity, All Rights Reserved | Powered by <a href="http://www.gravityecom.com/">Gravity Ecom Business Pvt. Ltd.</a>
	</td>
     <td> </td>
    </tr></tbody></table></font>
</div>
<!-- /END CONTAINER -->	
</footer>
<!-- /END FOOTER -->
<!-- =========================
     SCRIPTS 
============================== -->
<script src="js/bootstrap.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/matchMedia.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/custom.js"></script>
<script src="js/main.js"></script>
<script src="js/rotate.js"></script>
</body>
</html>

<?php
  // Database Connection

  $servername = "localhost";

  $username = "astro360user";

  $password = "astro360";

  $databse = "astro360";
  
  $conn = mysqli_connect($servername, $username, $password);
  
  mysqli_select_db($conn, $databse);

  // Reset Link
  if(isset($_POST['reset_password'])){

      $email = $_POST['email'];
      
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "<script>document.getElementById('invalid-email').style.display = 'block';</script>";        
      }
      
      else{

        $sql_query = "SELECT * FROM `user` WHERE `email`='$email'"; 
        
        $run_query = mysqli_query($conn, $sql_query);
        
        $count = mysqli_num_rows($run_query);
        
        if($count>0){
            $get_row = mysqli_fetch_array($run_query);
            $to = $get_row['email'];
            // // $from = 'amitdubey8888@gmail.com';
            // $subject = 'Contact Form';
            
            $name = $get_row['full_name'];
            $from = 'gravityecomhr@gmail.com';
            $subject = "Forgot Password!";
            $password = $get_row['password'];
            $message = '<html><body>';
            $message .= '<p>Dear <b>'.$name.'</b></p>';
            $message .= '<p>Your password associated with '.$to.' is <b>'.$password.'</b>.</p>';
            $message .= '<p>Kindly use this password for login.</p>';
            $message .= '<h4><i>Thank You</i></h4>';
            $message .= '<p>Astrosum Team</p>';            
            $message .= '</body></html>';
            
            $headers = 'From:'.'Astrosum Admin '.$from. "\r\n" ;
            $headers .='Reply-To: '. $from . "\r\n" ;
            $headers .='X-Mailer: PHP/' . phpversion();
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";   
            
            if(mail($to, $subject, $message, $headers)) 
            {
              echo "<script>document.getElementById('reset-link').style.display = 'block';</script>";            
            } 
            else 
            {
              echo "<script>document.getElementById('wrong-link').style.display = 'block';</script>";                          
            }
        }
        else{
          echo "<script>document.getElementById('no-account').style.display = 'block';</script>";          
        }
    }      
  }
?>