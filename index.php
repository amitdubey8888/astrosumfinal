<?php header("Access-Control-Allow-Origin: *"); ?>
<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from Astrosumhoroscope.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Jul 2017 11:30:13 GMT -->
<head>
<meta charset="UTF-8">
<title>Astrosum - Find Out Your Life.</title>
<meta name="description" content="Astro 360 is a Free Android application for your daily life Solutions based on Astrology calculations, you can submit question about your problems and get the answer in 24Hours">
<meta name="keywords" content="Astrology, Horoscope, 360, Free android application">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="google-site-verification" content="9eUtnfXGbPD6-CwACYBjkPDBYDZheFkJlPgtEeW9-D0" />
<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/astro_logo.jpg">
<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">
<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- FONT ICONS -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<link rel="stylesheet" href="assets/app-icons/styles.css">
<!--[if lte IE 7]><script src="lte-ie7.js"></script><![endif]-->
<!-- WEB FONTS -->
<link href="http://fonts.googleapis.com/css?family=Roboto:100,300,100italic,400,300italic" rel="stylesheet" type="text/css">
<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- ANIMATIONS -->
<link rel="stylesheet" href="css/animate.min.css">

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/blue.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">

<link rel="stylesheet" href="css/main-style.css">

<!-- JQUERY -->
<script src="js/validation.js"></script>
<script src="js/jquery.min.js"></script>

</head>

<body>
<script type="text/javascript" src="./fbapp/fb.js"></script>                    
<!-- =========================
     PRE LOADER       
============================== -->
<div class="preloader">
  <div class="status">&nbsp;</div>
</div>

<!-- =========================
     HEADER   
============================== -->
<header class="header" data-stellar-background-ratio="0.5" id="home">
	<!-- COLOR OVER IMAGE -->
	<div class="color-overlay"> 
		<!-- STICKY NAVIGATION -->
		<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
			<div class="container">
				<div class="navbar-header">
					<!-- LOGO ON STICKY NAV BAR -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kane-navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img width="120" height = "100" src="images/astro_logo.jpg" alt="Prediction"></a>				
				</div>	
				<!-- NAVIGATION LINKS -->
				<div class="navbar-collapse collapse" id="kane-navigation">
					<ul class="nav navbar-nav navbar-right main-navigation">
					<li><a href="#home">Home</a></li>
					<li><a href="#features">Features</a></li>
					<li><a href="#services">Services</a></li>
					<li><a href="#horoscope">Horoscope</a></li>
					<li><a href="#testimonial">Testimonials</a></li>
					<li><a href="#screenshot-section">Screenshots</a></li>
					<li><a href="#download">Download</a></li>
					<li><a href="#contactus">Contact Us</a></li>  
					</ul>
				</div>
			</div> <!-- /END CONTAINER -->
		</div> 
		<!-- /END STICKY NAVIGATION -->
		<!-- CONTAINER -->
		<div class="container">
			<!-- ONLY LOGO ON HEADER -->
			<div class="row">
				<div class="col" style="width:50%; margin:0px; float:left;">
					<div class="navbar" style="padding:0px; margin: 0px; padding-top: 15px; padding-bottom: 10px;">
						<div class="navbar-header">
							<img src="images/astro_logo.jpg" id="logo-image-style">
						</div>
					</div>
				</div> 
				<!-- Login Button Start-->
				<div class="col" style="width:50%; margin:0px; float:right;">
					<div class="navbar" style="float:right; padding:0px; margin: 0px; padding-top: 15px; padding-bottom: 10px;">
						<div class="navbar-header" id="navbar-header-style">
							<button id="login-button" onclick="loginRegisterModal();">LOGIN/REGISTER</button>
						</div>
					</div>
				</div>
				<!-- Login Button End -->
			</div>

			<script>
				function loginRegisterModal(){
					$('#loginsignupModal').modal('show');					
				}
			</script>
			
			<!-- Login Signup Modal Start -->

			<div class="modal-form" id="login-signup-modal">
				<div class="modal fade" id="loginsignupModal" data-backdrop="static" data-keyboard="false" style="z-index:999999 !important;">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header" style="width:100%;background:#FFA500 !important;color: aliceblue;padding-bottom: 0px; height:80px;">
								<p class="modal-title-style">Astrosum</p>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -35px;">
								<span aria-hidden="true">&times;</span>
								</button>
								<br>
							</div>
							<div class="row">
								<p id="login-success"></p>
								<p id="login-failed"></p>
								<div class="col-md-6">
									<div class="select-button-login-signup">
										<ul class="ul-class">
											<li class="li-class-first" onclick="openLoginForm()"><a id="login-a-style">LOGIN</a></li>
											<li class="li-class-second" onclick="openRegisterForm()"><a id="register-a-style">REGISTER</a></li>
										</ul>
									</div>
									<div class="modal-body">								
										<form id="login-form-display">
											<div class="form-group">
												<input type="text" class="form-control" onkeyup="validateEmail()" id="login_user_email" placeholder="Email Id">
												<small id="login_email" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Invalid Email.</small>
											</div>
											<div class="form-group">
												<input type="password" class="form-control" onkeyup="validatePassword()" id="login_user_password" placeholder="Password">
												<small id="login_password" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Password should be at least six charecter.</small>												
											</div>
											<a class="forgot-password" href="forgot-password.php">Forgot Password?</a>
										</form>
										<form id="register-form-display">
											<div class="form-group">
												<input type="text" class="form-control" onkeyup="validateName()" id="register_user_name" placeholder="Name">
												<small id="register_name" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>																								
											</div>
											<div class="form-group">
												<input type="text" class="form-control" onkeyup="validateEmail()" id="register_user_email" placeholder="Email">
												<small id="register_email" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Invalid Email.</small>												
											</div>
											<div class="form-group">
												<input type="date" class="form-control" onkeyup="validateDOB()" id="register_user_dob" placeholder="Date of Birth">
												<small id="register_dob" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>																								
											</div>
											<div class="form-group">
												<input type="password" class="form-control" onkeyup="validatePassword()" id="register_user_password" placeholder="Password">
												<small id="register_password" style="color:red;display:none;float:left;font-weight:600;font-size:small;">Password should be at least six charecter.</small>																								
											</div>
										</form>
										<div class="form-group" id="login-form-group-button">
											<button id="login-button-style" onclick="login()">LOGIN</button>
										</div>
										<div class="form-group" id="register-form-group-button">
											<button id="register-button-style" onclick="register()">REGISTER</button>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-12" id="facebook-login-colon">
										<div class="fb-login-button" data-scope="public_profile, email" onlogin="checkLoginState();" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true" style="margin-bottom: 5px;"></div>
									</div>
									<div class="col-md-12">
										<!-- <button id="social-login-button-google"><i class="fa fa-google-plus" aria-hidden="true" style="margin-right: 10px;"></i> Login with Google</button> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<script type="text/javascript">
				
				function openRegisterForm(){
					document.getElementById('login-form-display').style.display = 'none';									
					document.getElementById('login-form-group-button').style.display = 'none';											
					document.getElementById('register-form-display').style.display = 'block';									
					document.getElementById('register-form-group-button').style.display = 'block';									
				}
				function openLoginForm(){
					document.getElementById('register-form-display').style.display = 'none';									
					document.getElementById('register-form-group-button').style.display = 'none';										
					document.getElementById('login-form-display').style.display = 'block';									
					document.getElementById('login-form-group-button').style.display = 'block';
				}
				function login(){

					var email = document.getElementById('login_user_email').value;
					var password = document.getElementById('login_user_password').value;
					var user_email = document.getElementById('login_user_email').value;
					document.cookie = user_email;				
					if(email != '' && password != ''){
						$.ajax({                                      
							url: 'http://astrosum.com/en/astro360api/login.php',
							crossDomain: true,
							type: 'GET',								   							
							data: { 'email':email,
							        'password':password
								  },  
							success: function(data)
							{   
								document.getElementById('login_user_email').value = '';
								document.getElementById('login_user_password').value = '';
								var obj = JSON.parse(data);
								console.log(obj);								
								if(obj.error == 0){
									document.getElementById('login-success').style.display = 'block';
									document.getElementById('login-success').innerHTML = 'Login Successfull!';
									setInterval(loginTrueMessage, 5000);
									function loginTrueMessage(){
										document.getElementById('login-success').style.display = 'none';
									    $('#loginsignupModal').modal('hide');
									}
									document.write("<?php 
														session_start(); 
														$_SESSION['email'] = $_COOKIE['user_email']; 
														?>");
									window.location.href = "./welcome.php?email="+email;																		
								}
								else{
									document.getElementById('login-failed').style.display = 'block';
									document.getElementById('login-failed').innerHTML = 'Please Enter Correct Email & Password!';
									setInterval(loginFalseMessage, 3000);
									function loginFalseMessage(){
										document.getElementById('login-failed').style.display = 'none';
									}
								}																
							},
							error: function(error){
								console.log(error);
							} 
						});	
					}
					else
					{
						document.getElementById('login-failed').style.display = 'block';
						document.getElementById('login-failed').innerHTML = 'All Fields Are Mandatory.';
						setInterval(loginFalseMessage, 3000);
						function loginFalseMessage(){
							document.getElementById('login-failed').style.display = 'none';
						}
					}
				}
				function register(){

					var name = document.getElementById('register_user_name').value;
					var email = document.getElementById('register_user_email').value;
					var dob = document.getElementById('register_user_dob').value;
					var password = document.getElementById('register_user_password').value;
																
					if(name != '' && email != '' && dob != '' && password != ''){

						$.ajax({                                      
							url: 'http://astrosum.com/en/astro360api/register.php',
							type: 'POST',
							data: { 'name':name,
							        'email':email,
							        'dob':dob,
							        'password':password, 
									'device_id':'',
							      },   
							success: function(data)
							{   
								document.getElementById('register_user_name').value = '';
								document.getElementById('register_user_email').value = '';
								document.getElementById('register_user_dob').value = '';
								document.getElementById('register_user_password').value = '';
								var obj = JSON.parse(data);								
								if(obj.error == 0){
									document.getElementById('login-success').style.display = 'block';
									document.getElementById('login-success').innerHTML = 'Registration Successfull !';	
									setInterval(loginTrueMessage, 5000);
									function loginTrueMessage(){
										document.getElementById('login-success').style.display = 'none';
									    $('#loginsignupModal').modal('hide');
									}	
									document.write("<?php 
														session_start(); 
														$_SESSION['email'] = $_COOKIE['user_email']; 
														?>");
									window.location.href = "./welcome.php?email="+email;							
								}
								else{
									document.getElementById('login-failed').style.display = 'block';
									document.getElementById('login-failed').innerHTML = obj.message;
									setInterval(loginFalseMessage, 3000);
									function loginFalseMessage(){
										document.getElementById('login-failed').style.display = 'none';
									}
								}
							} 
						});	
					}
					else
					{
						document.getElementById('login-failed').style.display = 'block';
						document.getElementById('login-failed').innerHTML = 'All Fields Are Mandatory.';
						setInterval(loginFalseMessage, 3000);
						function loginFalseMessage(){
							document.getElementById('login-failed').style.display = 'none';
						}
					}
				}
			</script>

			<!-- Login Signup Modal End -->

			<!-- /END ONLY LOGO ON HEADER -->	
			
			<!-- Testing Wheel Start-->

			<style>
			
			@import url(https://fonts.googleapis.com/css?family=Lato:100,200,300,400,500,600);
			
			.ring-wrapper {
			position: relative;
			display: block;
			width: 500px;
			height: 500px;
			margin: 0 auto;
			overflow: hidden;
			top: -50px;
			}

			.ring {
			display: block;
			width: 100px;
			height: 100px;
			position: absolute;
			top: 0;
			transition: transform 0.25s, box-shadow 0.25s;
			overflow: hidden;
			border-radius: 100%;
			}

			.ring-display {
			width: 100%;
			height: 100%;
			display: block;
			border-radius: 100%;
			overflow: hidden;
			position: absolute;
			}

			.interaction {
			width: 100%;
			height: 100%;
			display: block;
			border-radius: 100%;
			position: absolute;
			cursor: pointer;
			}

			.ring:hover + .ring {
			transition: box-shadow 0.25s;
			}

			.layer-1 {
			width: 500px;
			height: 500px;
			border-radius: 100%;
			}

			.label {
			top: 50%;
			left:0%;
			width: 100%;
			height: 90%;
			text-align: center;
			transform-origin: 50% 0;
			position: absolute;
			//transform:rotate(0deg) translate(0, -50%);
			pointer-events: none;
			color: #000;
			text-shadow: 1px 1px 4px rgba(0, 0, 0, 0.5);
			line-height: 100px;
			font-size: 2em;
			transition: color 1s;
			-webkit-touch-callout: none;
			-webkit-user-select: none;
			-khtml-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			}

			.label:nth-child(1) {
				transform: rotate(0deg) translate(0, -50%);
			}

			.label:nth-child(2) {
				transform: rotate(30deg) translate(0, -50%);
			}

			.label:nth-child(3) {
				transform: rotate(60deg) translate(0, -50%);
			}

			.label:nth-child(4) {
				transform: rotate(90deg) translate(0, -50%);
			}

			.label:nth-child(5) {
				transform: rotate(120deg) translate(0, -50%);
			}

			.label:nth-child(6) {
				transform: rotate(150deg) translate(0, -50%);
			}

			.label:nth-child(7) {
				transform: rotate(180deg) translate(0, -50%);
			}

			.label:nth-child(8) {
				transform: rotate(210deg) translate(0, -50%);
			}

			.label:nth-child(9) {
				transform: rotate(240deg) translate(0, -50%);
			}

			.label:nth-child(10) {
				transform: rotate(270deg) translate(0, -50%);
			}

			.label:nth-child(11) {
				transform: rotate(300deg) translate(0, -50%);
			}

			.label:nth-child(12) {
				transform: rotate(330deg) translate(0, -50%);
			}
			.image-style{
				width:60px;
				height:60px;
				transform: rotate(180deg);
			}

			.center-image-style {
				width: 350px;
				height: 450px;
				position: absolute;
				top: 0px;
				left: 500px;
			}
			@media only screen and (min-width:1440px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 545px !important;
				}
			}
			@media only screen and (min-width:1367px){
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 575px !important;
				}
			}
			@media only screen and (max-width:1125px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 380px;
				}
			}
			@media only screen and (max-width:1120px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 375px;
				}
			}
			@media only screen and (max-width:1100px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 368px;
				}
			}
			@media only screen and (max-width:1096px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 365px;
				}
			}
			@media only screen and (max-width:1085px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 360px;
				}
			}
			@media only screen and (max-width:1075px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 355px;
				}
			}
			@media only screen and (max-width:1060px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 348px;
				}
			}
			@media only screen and (max-width:1050px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 342px;
				}
			}
			@media only screen and (max-width:1040px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 337px;
				}
			}
			@media only screen and (max-width:1030px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 332px;
				}
			}
			@media only screen and (max-width:1025px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 330px;
				}
			}
			@media only screen and (max-width:1010px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 324px;
				}
			}
			@media only screen and (max-width:1000px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 318px;
				}
			}
			@media only screen and (max-width:996px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 316px;
				}
			}
			@media only screen and (max-width:992px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 310px;
				}
			}
			@media only screen and (max-width:980px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 308px;
				}
			}
			@media only screen and (max-width:970px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 302px !important;
				}
			}
			@media only screen and (max-width:960px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 298px !important;
				}
			}
			@media only screen and (max-width:950px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 292px !important;
				}
			}
			@media only screen and (max-width:940px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 288px !important;
				}
			}
			@media only screen and (max-width:930px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 282px !important;
				}
			}
			@media only screen and (max-width:920px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 275px !important;
				}
			}
			@media only screen and (max-width:900px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 265px !important;
				}
			}
			@media only screen and (max-width:880px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 255px;
				}
			}
			@media only screen and (max-width:860px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 245px !important;
				}
			}
			@media only screen and (max-width:860px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 235px !important;
				}
			}
			@media only screen and (max-width:840px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 225px !important;
				}
			}
			@media only screen and (max-width:820px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 215px !important;
				}
			}
			@media only screen and (max-width:780px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 206px !important;
				}
			}
			@media only screen and (max-width:760px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 196px !important;
				}
			}
			@media only screen and (max-width:750px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 192px !important;
				}
			}
			@media only screen and (max-width:740px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 186px !important;
				}
			}
			@media only screen and (max-width:730px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 182px !important;
				}
			}
			@media only screen and (max-width:720px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 178px !important;
				}
			}
			@media only screen and (max-width:700px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 168px !important;
				}
			}
			@media only screen and (max-width:690px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 162px !important;
				}
			}
			@media only screen and (max-width:680px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 158px !important;
				}
			}
			@media only screen and (max-width:660px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 148px !important;
				}
			}
			@media only screen and (max-width:640px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 138px !important;
				}
			}
			@media only screen and (max-width:630px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 133px !important;
				}
			}
			@media only screen and (max-width:620px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 128px !important;
				}
			}
			
			@media only screen and (max-width:590px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 110px !important;
				}
			}
			@media only screen and (max-width:570px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 102px !important;
				}
			}
			@media only screen and (max-width:560px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 98px !important;
				}
			}
			@media only screen and (max-width:550px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 92px !important;
				}
			}
			@media only screen and (max-width:500px) {
				.center-image-style {
					position: absolute;
					top: 0px;
					left: 58px !important;
				}
				.label{
					position: absolute;
					left:-6%;
				}
			}
			.sticky-navigation .navbar-header img {
					max-height: 70px;
					position: relative;
					width: 85px;
					left: 0px;
					top: -25px;
			}
			@media only screen and (max-width:480px) {
				.sticky-navigation .navbar-header img {
					max-height: 65px;
					position: relative;
					width: 85px;
					left: 15px;
					top: -15px;
				}
				#logo-image-style {
					width: 100px;
					height: 100px;
					position: relative;
					top: -15px;
					left: -15px;
				}
				a.navbar-brand{
					padding:0px !important;
				}
				.center-image-style {
					position: absolute;
					top: 70px;
					left: 105px !important;
					width: 280px;
					height: 330px;
				}
				.label{
					position: absolute;
					left:-7%;
				}
				.button-horoscope-style {
					margin: auto;
					width: 60%;
				}
				.ring-wrapper {
					width: 450px !important;
					height: 425px !important;
				}
				.home-contents {
    				margin-top:0% !important;
				}
				h3.intro.text-center{
					font-size:15px !important;
				}
				.layer-1 {
					width: 450px !important;
				}
				.image-style {
					width: 40px;
					height: 40px;
				}
				.label {
					position: absolute;
					left: 1% !important;
					top: 45% !important;
					width: 100% !important;
					height: 75% !important;
				}
				#login-button {
					margin-right:10px !important;
				}
				.navbar-header {
    				/* padding-top: 20px; */
				}
				#card-background {
    				margin-right: 0px !important;
    				width: 85% !important;
				}
				.modal-dialog {
					width: 93% !important;
					margin-top: 30px !important;
				}
				.card-col-style {
					width: 70%;
					display: inline-block;
				}
				#login-button-style, #register-button-style {
					position: relative;
					width: 250px !important;
					left: 0px !important;
					margin-top: 0px !important;
				}
				#facebook-login-colon {
    				padding-top: 0px;
				}

			}
			@media only screen and (max-width:470px) {
				.ring-wrapper, .layer-1 {
					width: 440px !important;
				}
				.center-image-style {
					position: absolute;
					top: 70px;
					left: 99px !important;
				}
			}			
			@media only screen and (max-width:460px) {
				.center-image-style {
					position: absolute;
					top: 75px;
					left: 90px !important;
				}
				.label {
					position: absolute;
					left: 0% !important;
					top:42% !important;
				}
				.ring-wrapper, .layer-1 {
					width: 430px !important;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}
			@media only screen and (max-width:440px) {
				.center-image-style {
					position: absolute;
					left: 123px !important;
					top: 120px !important;
					width: 200px;
					height: 250px;
				}
				.image-style {
					width: 40px !important;
					height: 40px !important;
				}
				.label {
					top: 45% !important;
					left: 18% !important;
					width: 65% !important;
					height: 65% !important;
					margin: 0px;
					padding: 0px;
				}
				header .home-contents {
    				margin-top: -40%;
				}
				.button-horoscope-style {
					width: 75%;
					font-size: 15px;
				}
				.ring-wrapper, .layer-1{
					width:412px !important;
					height:450px;
				}
				.layer-1{
					width: 290px;
    				height: 450px;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
				}
				.home-contents {
					margin-top: -25% !important;
				}
				#navbar-header-style {
					margin-top: 20px !important;
					margin-right:10px !important;
				}
				#row-horoscope-style {
					position: relative !important;
					top: -140px !important;
				}
				h3.intro.text-center {
					font-size: 15px !important;
					padding: 20px;
				}
				#card-background {
					width: 100%;
					margin-left: 0px;
				}
				.card-col-style-1 {
					width: 85% !important;
				}
				.card-col-style {
					width: 80%;
				}
				#loginsignupModal {
					width: 96%;
				}
				#login-button-style, #register-button-style {
					width: 80%;
					left: 0px;
					margin-top:0px;
				}
				#facebook-login-colon {
    				padding-top: 0px;
				}
				.select-button-login-signup {
					padding-top: 15px;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}
			@media only screen and (max-width: 400px){
				#card-background {
					margin-left: 15px !important;
					width: 90% !important;
				}
				.modal-dialog {
					width: 92% !important;
					margin-top: 30px !important;
				}
				.center-image-style {
					position: absolute;
					left: 99px !important;
					top: 120px !important;
				}
				.ring-wrapper, .layer-1 {
					width: 365px !important;
				}
				.label {
					left: 17% !important;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}
			@media only screen and (max-width: 380px){
				.center-image-style {
					position: absolute;
					left: 96px !important;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}						
			@media only screen and (max-width: 360px){
				.center-image-style {
					position: absolute !important;
					left: 70px !important;
					top: 80px !important;
					width: 225px !important;
					height: 275px !important;
				} 
				.ring-wrapper, .layer-1 {
					width: 325px !important;
				}
				.modal-dialog {
    				width: 94% !important;
				}
				.label {
					top: 42% !important;
					left: 18% !important;
					height: 68% !important;
				}
				#getpredictionModal {
					width: 96%;
				}
				#readmoreModal {
    				width: 96%;
				}
				#horoscopeModal {
    				width: 96%;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}
			@media only screen and (max-width: 340px){
				.center-image-style{
					position: absolute !important;
    				left: 62px !important;
				}
				.label {
					left: 17% !important;
				}
				.modal-dialog {
					width: 95% !important;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}			
			@media only screen and (max-width: 320px){
				.ring-wrapper, .layer-1 {
   					 width: 292px !important;
				}
				#login-button {
    				margin-right: 0px !important;
				}
				.label {
    				position: absolute;
    				left: 19% !important;
				}
				.center-image-style {
					position: absolute !important;
					left: 50px !important;
					top: 80px !important;
					width: 225px !important;
					height: 275px !important;
				}
				.intro {
					margin-bottom: 16px;
					padding: 10px;
					font-size: larger;
				}
				#logo-image-style {
					position: relative;
					top: -25px;
					left: 0px;
				}
			}
			</style>   
			
			<div class="ring-wrapper">
			<div id="layer-1" class="ring layer-1">
			<div class="ring-display">
				
				<div class="label"><img class="image-style" id="image-style" src="category-logo/year.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/vivah.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/relationship.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/pastlife.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/hourly.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/precaution.png"></div>

				<div class="label"><img class="image-style" id="image-style" src="category-logo/medical.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/favourite.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/life.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/kidscorner.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/financial.png"></div>
				<div class="label"><img class="image-style" id="image-style" src="category-logo/certinity.png"></div>
				     
			</div>
			<div class="interaction"></div>
			</div>
			</div>
			<div id="div-center" style="z-index: 1000;"><img class="center-image-style" src="image/ganesha-logo.png"></div>						
			<script>

			circle('layer-1');

			function circle(id) {
			
			var horoscopelist = ["Astro Medical Chart","My Favourites","Life 360","Career and Profession","Life 360","Astro Financial Chart","Certainty 360","Year 360","Vivah Sutra","Relationship Compatibility","Past Life","Hourly Prediction","My Favourites"];
			
			var counter = 0;

			var el = document.getElementById(id);

			var elDisplay = el.children[0];
			
			var elInteraction = el.children[1];

			var offsetRad = null;
			var targetRad = 0;
			var previousRad;

			try {
				elInteraction.addEventListener('mousedown', down);
			}
			catch (err) {
				console.log("Interaction not found");
			}

			function down(event) {
				offsetRad = getRotation(event);
				previousRad = offsetRad;
				window.addEventListener('mousemove', move);
				window.addEventListener('mouseup', up);
			}

			function move(event) {
				var newRad = getRotation(event);
				targetRad += (newRad - previousRad);
				previousRad = newRad;
				elDisplay.style.transform = 'rotate(' + (targetRad / Math.PI * 180) + 'deg)';
				var elem2 = document.getElementById("category-button");

				// New Code For Circle Image
				console.log(Math.abs(parseInt(targetRad / Math.PI * 180).toFixed(0)))
				var remainder = (Math.abs(parseInt(targetRad / Math.PI * 180).toFixed(0)))%30;
				document.getElementById('image-style').style.transform = 'rotate(' + (Math.abs(parseInt(targetRad / Math.PI * 180))) + 'deg)';
				if(remainder == 0){
					elem2.innerHTML = horoscopelist[counter];
					if(counter == 11){
						counter=0;
					}else{
						counter++;
					}
				}
						
			}

			function up() {
				window.removeEventListener('mousemove', move);
				window.removeEventListener('mouseup', up);
			}

			function getRotation(event) {
				var pos = mousePos(event, elInteraction);
				var x = pos.x - elInteraction.clientWidth * .5;
				var y = pos.y - elInteraction.clientHeight * .5;
				return Math.atan2(y, x);
			}

			function mousePos(event, currentElement) {
				var totalOffsetX = 0;
				var totalOffsetY = 0;
				var canvasX = 0;
				var canvasY = 0;

				do {
					totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
					totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
				}
				while (currentElement = currentElement.offsetParent)

				canvasX = event.pageX - totalOffsetX;
				canvasY = event.pageY - totalOffsetY;

				return {
					x: canvasX,
					y: canvasY
				};
			}
			}
			</script>

			<!-- Testing Wheel End -->

			<div class="row" id="row-horoscope-style">
				
				<div class="col-md-12">
					<button onclick="changeButtonValue()" class="form-control button-horoscope-style text-center" id="category-button">Astro Medical Chart</button>
				</div>
				
			</div>
			<script type="text/javascript">
				function changeButtonValue(){						
					
					var indexValue = 0;
					
					var innerValue = document.getElementById('category-button').innerHTML;
					
					document.getElementById('modal-title').innerHTML = innerValue;
					
					if(innerValue == "Astro Financial Chart"){
						indexValue = 1;
					}
					else if(innerValue == "Astro Medical Chart"){
						indexValue = 2;
					}
					else if(innerValue == "Relationship Compatibility"){
						indexValue = 3;
						document.getElementById('pname1').style.display = 'block';
						document.getElementById('dob1').style.display = 'block';
						document.getElementById('pname2').style.display = 'block';
						document.getElementById('dob2').style.display = 'block';
						document.getElementById('your_name_category').style.display = 'none';
					}
					else if(innerValue == "Certainty 360"){
						indexValue = 4;
						document.getElementById('question').style.display = 'block';
						var type_value = document.getElementById('get_user_question').value;	
						if(type_value == ''){
							document.getElementById('category_question').style.display = 'block';
						}else{
							document.getElementById('category_question').style.display = 'none';
						}
					}
					else if(innerValue == "Career and Profession"){
						indexValue = 5;
					}
					else if(innerValue == "Life 360"){
						indexValue = 6;
					}
					else if(innerValue == "Vivah Sutra"){
						indexValue = 7;
						document.getElementById('mname').style.display = 'block';
						document.getElementById('mdob').style.display = 'block';
						document.getElementById('fname').style.display = 'block';
						document.getElementById('fdob').style.display = 'block';
						document.getElementById('your_name_category').style.display = 'none';
					}
					else if(innerValue == "Year 360"){
						indexValue = 8;
						document.getElementById('type').style.display = 'block';						
						var type_value = document.getElementById('get_user_type').value;	
						if(type_value == ''){
							document.getElementById('category_type_display').style.display = 'block';
						}
					}
					else if(innerValue == "Past Life"){
						indexValue = 9;
					}
					else if(innerValue == "My Favourites"){
						indexValue = 10;
					}
					else if(innerValue == "Hourly Prediction"){
						indexValue = 11;
					}
					
					$('#get_user_category option')[indexValue].selected = true;
					
					$('#getpredictionModal').modal('toggle');
					
				}
			</script>
			<style>
				body.modal-open{
					width:100%!important;
					padding-right:0!important;
					overflow-y:scroll!important;
					position:fixed!important;
				}
				.modal{
					overflow-y: hidden !important;
				}
				.modal-dialog{
					overflow-y: initial !important
				}
				#form-one-style{
					height: 500px;
					overflow-y: scroll;
    				overflow-x: hidden;
				}
				#detailModal{
					overflow-y: scroll !important;
    				overflow-x: hidden !important;
				}
			</style>   
			<div class="modal-form" id="image-style">
				<div class="modal fade" id="getpredictionModal" data-backdrop="static" data-keyboard="false">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header" style="width:100%;background:#FFA500 !important;color: aliceblue;padding-bottom: 0px; height:60px;">
								<h5 class="modal-title" id="modal-title"></h5>
								<h5 class="modal-result" id="modal-result">Your Result</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
								<span aria-hidden="true">&times;</span>
								</button>
								<br>
							</div>
							<div class="modal-body" style="width:100%;">
								<!--Details Modal Start-->
								<div class="modal fade" id="detailModal" data-backdrop="static" data-keyboard="false">
									<div class="modal-dialog" role="document" style="z-index: 999;width: 100%;box-shadow: 5px 5px 5px 5px darkgrey;top:-30px;">
										<!-- Modal content-->
										<div class="modal-content">
											<div class="modal-header" style="background:#FFA500 !important; color:aliceblue;">
												<h4 id="modal-title-day"></h4>						
											</div>
											<div class="modal-body">
												<table id="category_day" class="table table-striped">
													<thead>
														<tr>
															<th style="width:10%;text-align:center;">Day</th>
															<th style="width:10%;text-align:center;">Percentage</th>
															<th style="width:10%;text-align:center;">Color</th>
														</tr>
													</thead>
													<tbody id="table_body_day" class="table-body">
													</tbody>
												</table>
											</div>
											<div class="modal-footer">
												<p class="btn btn-success" onclick="closeDetailModal();" style="background:#FFA500 !important;color:aliceblue;">Close</p>
											</div>
										</div>
									</div>
								</div> 
								<!--Details Modal End  -->
								<script>
									function closeDetailModal(){
										// $('#detailModal').modal('hide');
										$('#detailModal').css('display', 'none');	
										$('#table_body_day tr td').css("display", "none");											
									}
								</script>
								<form id="form-one-style" style="display:none;">
									<table id="category128" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-tag"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">S.N.</th>
												<th style="width:10%;text-align:center;">Month</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
												<th style="width:10%;text-align:center;" id="detailHeight">Details</th>										
											</tr>
										</thead>
										<tbody id="table-body-1" class="table-body">
										</tbody>
									</table>

									<table id="category12" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-12"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">S.N.</th>
												<th style="width:10%;text-align:center;">Time</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-2" class="table-body">	
										</tbody>
									</table>

									<table id="relationship" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-3"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">Quote</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-3" class="table-body">

										</tbody>
									</table>

									<div id="certainty" style="display:none;">
										<h4 class="text-center" id="category-4"></h4>
										<div class="text-center">
											<p id="certainty360"></p>
										</div>										
									</div>
									<div id="pastlife" style="display:none;">
										<h4 class="text-center" id="category-9"></h4>
										<div class="past-life-text">
											<p id="past-life1" style="text-align:left;"></p>
											<p id="past-life2" style="text-align:left;"></p>
											<p id="past-life3" style="text-align:left;"></p>										
										</div>										
									</div>
									
									<table id="kidcareer" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-5"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">Quote</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-4" class="table-body">

										</tbody>
									</table>

									<table id="life" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-6"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">Quote</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-5" class="table-body">

										</tbody>
									</table>

									<table id="vivah" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-7"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">Quote</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-6" class="table-body">

										</tbody>
									</table>

									<table id="pain_gain" class="table table-striped" style="display:none;">
										<h4 class="text-center" id="category-10"></h4>										
										<thead>
											<tr>
												<th style="width:10%;text-align:center;">Quote</th>
												<th style="width:10%;text-align:center;">Percentage</th>
												<th style="width:10%;text-align:center;">Color</th>
											</tr>
										</thead>
										<tbody id="table-body-7" class="table-body">

										</tbody>
									</table>

									<div id="my_favourite" style="display:none;">
										<h4 class="text-center" id="category-11"></h4>
										<div class="row">
											<div class="col-md-12">
												<p id="my_favourite1" style="text-align: left;"></p>
												<p id="my_favourite2" style="text-align: left;"></p>
												<p id="my_favourite3" style="text-align: left;"></p>
												<p id="my_favourite4" style="text-align: left;"></p>
												<p id="my_favourite5" style="text-align: left;"></p>
												<p id="my_favourite6" style="text-align: left;"></p>
												<p id="my_favourite7" style="text-align: left;"></p>
												<p id="my_favourite8" style="text-align: left;"></p>
												<p id="my_favourite9" style="text-align: left;"></p>
												<p id="my_favourite10" style="text-align: left;"></p>
												<p id="my_favourite11" style="text-align: left;"></p>												
											</div>																								
										</div>										
									</div>
								</form>
								<form id="form-two-style"> 
									<h4 id="mandatory_field" style="display:none; text-align:center;">All fields are mandatory.</h4>
									<div class="form-group">
										<label for="get_user_category" class="form-control-label" style="float:left;color: coral;">Select Your Category</label>
										<select class="form-control" onchange="showForm()" id="get_user_category" disabled>
										<option value="">Select Category</option>
										<option value="1">Astro Financial Chart</option>
										<option value="2">Astro Medical Chart</option>
										<option value="3">Relationship Compatibility</option>
										<option value="4">Certainty 360</option>
										<option value="5">Career and Profession</option>
										<option value="6">Life 360</option>
										<option value="7">Vivah Sutra</option>
										<option value="8">Year 360</option>
										<option value="9">Past Life</option>
										<option value="11">My Favourites</option>
										<option value="12">Hourly Prediction</option>
										</select>
									</div>
									<script>
										function showForm(){
											var category_id = document.getElementById('get_user_category').value;
											if(category_id == 3){
												document.getElementById('pname1').style.display = 'block';
												document.getElementById('dob1').style.display = 'block';
												document.getElementById('pname2').style.display = 'block';
												document.getElementById('dob2').style.display = 'block';

												document.getElementById('question').style.display = 'none';
												
												document.getElementById('type').style.display = 'none';												

												document.getElementById('mname').style.display = 'none';
												document.getElementById('mdob').style.display = 'none';
												document.getElementById('fname').style.display = 'none';
												document.getElementById('fdob').style.display = 'none';

											}
											else if(category_id == 4){
												document.getElementById('question').style.display = 'block';

												document.getElementById('type').style.display = 'none';

												document.getElementById('pname1').style.display = 'none';
												document.getElementById('dob1').style.display = 'none';
												document.getElementById('pname2').style.display = 'none';
												document.getElementById('dob2').style.display = 'none';		

												document.getElementById('mname').style.display = 'none';
												document.getElementById('mdob').style.display = 'none';
												document.getElementById('fname').style.display = 'none';
												document.getElementById('fdob').style.display = 'none';										
											}
											else if(category_id == 7){
												document.getElementById('mname').style.display = 'block';
												document.getElementById('mdob').style.display = 'block';
												document.getElementById('fname').style.display = 'block';
												document.getElementById('fdob').style.display = 'block';
												
												document.getElementById('type').style.display = 'none';
												
												document.getElementById('question').style.display = 'none';												
												
												document.getElementById('pname1').style.display = 'none';
												document.getElementById('dob1').style.display = 'none';
												document.getElementById('pname2').style.display = 'none';
												document.getElementById('dob2').style.display = 'none';
											}
											else if(category_id == 8){
												document.getElementById('type').style.display = 'block';

												document.getElementById('pname1').style.display = 'none';
												document.getElementById('dob1').style.display = 'none';
												document.getElementById('pname2').style.display = 'none';
												document.getElementById('dob2').style.display = 'none';
												
												document.getElementById('question').style.display = 'none';
												
												document.getElementById('mname').style.display = 'none';
												document.getElementById('mdob').style.display = 'none';
												document.getElementById('fname').style.display = 'none';
												document.getElementById('fdob').style.display = 'none';												
																								
											}
											else{
												document.getElementById('type').style.display = 'none';

												document.getElementById('pname1').style.display = 'none';
												document.getElementById('dob1').style.display = 'none';
												document.getElementById('pname2').style.display = 'none';
												document.getElementById('dob2').style.display = 'none';
												
												document.getElementById('question').style.display = 'none';
												
												document.getElementById('mname').style.display = 'none';
												document.getElementById('mdob').style.display = 'none';
												document.getElementById('fname').style.display = 'none';
												document.getElementById('fdob').style.display = 'none';	
											}
										}
									</script>
									<div class="form-group" id="type" style="display:none;">
										<label for="get_user_type" class="form-control-label" style="float:left;color: coral;">Select Type</label>
										<select class="form-control" onchange="hideerror()" id="get_user_type">
										<option value="">Select Type</option>
										<option value="1">Career & Education</option>
										<option value="2">Emotional Stability </option>
										<option value="3">Family</option>
										<option value="4">Finance & Profits</option>
										<option value="5">Health</option>
										<option value="6">Merried Life</option>
										<option value="7">Power & Higher Authority</option>
										<option value="8">Property & Vehicle </option>
										<option value="9">Respect Gains & Status</option>
										<option value="10">Spirituality & Travels</option>
										</select>
										<small id="category_type_display" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Type is Required.</small>										
									</div>
									<script>
										function hideerror(){
											var type_value = document.getElementById('get_user_type').value;	
											if(type_value == ''){
												document.getElementById('category_type_display').style.display = 'block';
											}else{
												document.getElementById('category_type_display').style.display = 'none';
											}
										}
									</script>
									<div class="form-group" id="mname" style="display:none;">
										<label for="show_user_mname" class="form-control-label" style="float:left;color: coral;">Male Name</label>
										<input type="text" class="form-control" onkeyup="validateName()" id="get_user_mname">
										<small id="category_mname" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>
									</div>
									<div class="form-group" id="mdob" style="display:none;">
										<label for="show_user_mdob" class="form-control-label" style="float:left;color: coral;">Male DOB</label>
										<input type="date" class="form-control" onkeyup="validateDOB()" id="get_user_mdob">
										<small id="category_mdob" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
									</div>
									<div class="form-group" id="fname" style="display:none;">
										<label for="show_user_fname" class="form-control-label" style="float:left;color: coral;">Female Name</label>
										<input type="text" class="form-control" onkeyup="validateName()" id="get_user_fname">
										<small id="category_fname" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>
									</div>
									<div class="form-group" id="fdob" style="display:none;">
										<label for="show_user_fdob" class="form-control-label" style="float:left;color: coral;">Female DOB</label>
										<input type="date" class="form-control" onkeyup="validateDOB()" id="get_user_fdob">
										<small id="category_fdob" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
									</div>
									<div class="form-group" id="question" style="display:none;">
										<label for="show_user_question" class="form-control-label" style="float:left;color: coral;">Ask Your Question</label>
										<input type="text" class="form-control" onkeyup="validateQuestion()" id="get_user_question">
										<small id="category_question" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please fill out this field.</small>
									</div>
									<script>
										function validateQuestion(){
											var type_value = document.getElementById('get_user_question').value;	
											if(type_value == ''){
												document.getElementById('category_question').style.display = 'block';
											}else{
												document.getElementById('category_question').style.display = 'none';
											}
										}
									</script>
									<div class="form-group" id="pname1" style="display:none;">
										<label for="show_user_pname1" class="form-control-label" style="float:left;color: coral;">Partner One Name</label>
										<input type="text" class="form-control" onkeyup="validateName()" id="get_user_pname1">
										<small id="category_pname1" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>
									</div>
									<div class="form-group" id="dob1" style="display:none;">
										<label for="show_user_dob1" class="form-control-label" style="float:left;color: coral;">Partner One DOB</label>
										<input type="date" class="form-control" onkeyup="validateDOB()" id="get_user_dob1">
										<small id="category_dob1" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
									</div>
									<div class="form-group" id="pname2" style="display:none;">
										<label for="show_user_pname2" class="form-control-label" style="float:left;color: coral;">Partner Second Name</label>
										<input type="text" class="form-control" onkeyup="validateName()" id="get_user_pname2">
										<small id="category_pname2" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>										
									</div>
									<div class="form-group" id="dob2" style="display:none;">
										<label for="show_user_dob2" class="form-control-label" style="float:left;color: coral;">Partner Second DOB</label>
										<input type="date" class="form-control" onkeyup="validateDOB()" id="get_user_dob2">
										<small id="category_dob2" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
									</div>
									<div class="form-group">
										<label for="show_user_email" class="form-control-label" style="float:left;color: coral;">Your Email</label>
										<input type="text" class="form-control" onkeyup="validateEmail()" id="get_user_email">
										<small id="category_email" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Invalid Email.</small>
									</div>
									<div class="form-group" id="your_name_category">
										<label for="get_user_name" class="form-control-label" style="float:left;color: coral;">Your Name</label>
										<input type="text" class="form-control" onkeyup="validateName()" id="get_user_name">
										<small id="category_name" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>										
									</div>
									<div class="form-group">
										<label for="get_user_dob" class="form-control-label" style="float:left;color: coral;">Your Date of Birth</label>
										<input type="date" class="form-control" onkeyup="validateDOB()" id="get_user_dob">
										<small id="category_dob" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
									</div>
								</form>
							</div>
							<div class="modal-footer" id="modal-footer" style="width:100%; margin-top:0px;">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								<button id="get-user-value" onclick="getUserDatabase()" class="btn btn-success" style="background:#FFA500 !important;">Submit</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<script type="text/javascript">
				
				function getUserDatabase(){
					var email = document.getElementById('get_user_email').value;
					var name = document.getElementById('get_user_name').value;
					var dob = document.getElementById('get_user_dob').value;
					var category = document.getElementById('get_user_category').value;
					
					var question = document.getElementById('get_user_question').value;
					
					var type = document.getElementById('get_user_type').value;					

					var mname = document.getElementById('get_user_mname').value;
					var newmdob = document.getElementById('get_user_mdob').value;
					var fname = document.getElementById('get_user_fname').value;
					var newfdob = document.getElementById('get_user_fdob').value;
					
					var spliteddobm = newmdob.split("-");
					var spliteddobf = newfdob.split("-");
					var mdob = spliteddobm[2]+spliteddobm[1]+spliteddobm[0];
					var fdob = spliteddobf[2]+spliteddobf[1]+spliteddobf[0];

					var pname1 = document.getElementById('get_user_pname1').value;
					var newdob1 = document.getElementById('get_user_dob1').value;
					var pname2 = document.getElementById('get_user_pname2').value;
					var newdob2 = document.getElementById('get_user_dob2').value;
					var spliteddob1 = newdob1.split("-");
					var spliteddob2 = newdob2.split("-");
					var dob1 = spliteddob1[2]+spliteddob1[1]+spliteddob1[0];
					var dob2 = spliteddob2[2]+spliteddob2[1]+spliteddob2[0];
					
					var spliteddob = dob.split("-");					
					var newdob = spliteddob[2]+spliteddob[1]+spliteddob[0];
					
					var splitedname = name.split(" ");
					var fname = splitedname[0];					
					var newdd = spliteddob[2];
					var newmm = spliteddob[1];
					var newyyyy = spliteddob[0];					
					var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
					
					if(category == 1)
					{
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/afc';	//Done
						var dataContent = {
							'name':name,
							'dob':newdob
						} 				
					}
					else if(category == 2){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/amc';	//Done	
						var dataContent = {
							'name':name,
							'dob':newdob
						} 									
					}
					else if(category == 3){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/partner'; //Done
						var dataContent = {
							'pname1':pname1,
							'dob1':dob1,
							'pname2':pname2,
							'dob2':dob2
						} 
					}
					else if(category == 4){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/certainty'; //Done
						var dataContent = {
							'name':name,
							'dob':newdob,
							'question':question
						} 							
					}
					else if(category == 5){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/kidcareer'; //Done
						var dataContent = {
							'name':name,
							'dob':newdob
						} 								
					}
					else if(category == 6){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/life360';	//Done	
						var dataContent = {
							'name':name,
							'dob':newdob
						} 							
					}
					else if(category == 7){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/vivahsutra'; //Done
						var dataContent = {
							'mname':mname,
							'mdob':mdob,
							'fname':fname,
							'fdob':fdob
						} 								
					}
					else if(category == 8){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/year360';	//Done
						var dataContent = {
							'name':name,
							'dob':newdob,
							'type':type
						} 								
					}
					else if(category == 9){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/pastlife';	//Done
						var dataContent = {
							'fname':fname,
							'dd':newdd,
							'mm':newmm,
							'yyyy':newyyyy
						} 									
					}
					else if(category == 10){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/annualpans';	//Done
						var dataContent = {
							"name":name,
							'dob':newdob
						} 					
					}
					else if(category == 11){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/myfavourites'; //	Done
						var dataContent = {
							"name":name,
							'dob':newdob
						} 						
					}
					else if(category == 12){
						var api_url = 'http://astrosum.com/astrosum_api/index.php/Astrosumapp/horapred';	//Done	
						var dataContent = {
							"name":name,
							'dob':newdob
						} 									
					}

					if(email != '' && dob != '' && category != '')
					{
						$.ajax({                                      
							url: api_url,
							crossDomain: true,
							type: 'GET',
							data: dataContent,			
							success: function(data)
							{   
								document.getElementById('form-two-style').style.display = 'none';
								document.getElementById('modal-title').style.display = 'none';																
								document.getElementById('modal-footer').style.display = 'none';	
								document.getElementById('modal-result').style.display = 'block';																								
								document.getElementById('form-one-style').style.display = 'block';
								var varArr = new Array();								
								if(category == 1 || category == 2 || category == 8){
									varArr.push(Math.round(data[0].month1 * 1000) / 100); 
									varArr.push(Math.round(data[0].month2 * 1000) / 100); 
									varArr.push(Math.round(data[0].month3 * 1000) / 100); 
									varArr.push(Math.round(data[0].month4 * 1000) / 100); 
									varArr.push(Math.round(data[0].month5 * 1000) / 100); 
									varArr.push(Math.round(data[0].month6 * 1000) / 100); 
									varArr.push(Math.round(data[0].month7 * 1000) / 100); 
									varArr.push(Math.round(data[0].month8 * 1000) / 100); 
									varArr.push(Math.round(data[0].month9 * 1000) / 100); 
									varArr.push(Math.round(data[0].month10 * 1000) / 100); 
									varArr.push(Math.round(data[0].month11 * 1000) / 100); 
									varArr.push(Math.round(data[0].month12 * 1000) / 100); 
								}
								if(category == 1 || category == 2 || category == 8)
								{
									document.getElementById('category128').style.display = 'block';	
									if(category == 1){
										document.getElementById('category-tag').innerHTML = 'Astro Financial Chart';
									}
									else if(category == 2){
										document.getElementById('category-tag').innerHTML = 'Astro Medical Chart';
									}	
									else if(category == 8){
										document.getElementById('category-tag').innerHTML = 'Year 360';
									}								
									
									var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
									var now = new Date();
									var newMonthIndex = now.getMonth();						
									
									var i=11, j=12, k=11;
									for(var x=1; x <=12; x++){
										var thisMonth = months[--newMonthIndex];
										if(newMonthIndex == 0) {newMonthIndex = 12};
										var monthName = thisMonth;

										var table = document.getElementById("table-body-1");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);
										var cell4 = row.insertCell(3);
										var cell5 = row.insertCell(4);
																				
										cell1.innerHTML = (j--)+'.';
										cell2.innerHTML = monthName;
										cell3.innerHTML = varArr[i]+'%';
										if(varArr[i] > 0 && varArr[i] <= 48)
										{ 
											cell4.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(varArr[i] > 48 && varArr[i] <= 60)
										{
											cell4.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(varArr[i] > 60 && varArr[i] <= 100)
										{
											cell4.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										i--;
										if(category == 1 || category == 2){
											cell5.innerHTML = '<p class="btn btn-success" style="width:90px;" id="showthirdform">'+monthName+'</p>';
										}else{
											cell5.innerHTML = '<p>None</p>';											
										}
										if(category == 1){
											var day_api = "http://astrosum.com/astrosum_api/index.php/Astrosumapp/afcday";
										}
										if(category == 2){
											var day_api = "http://astrosum.com/astrosum_api/index.php/Astrosumapp/amcday";											
										}
										$(document).ready(function(){
											$('#showthirdform').click(function(){
												$('#detailModal').css('display','block');   												
												var month_name = $(this).html();
												console.log(month_name);
												document.getElementById('modal-title-day').innerHTML = month_name+' Month Details';
												
												if(month_name == "January"){
													var day_data = {'name':name,'dob':newdob,'month':0}
												}
												if(month_name == "February"){
													var day_data = {'name':name,'dob':newdob,'month':1}
												}
												if(month_name == "March"){
													var day_data = {'name':name,'dob':newdob,'month':2}
												}
												if(month_name == "April"){
													var day_data = {'name':name,'dob':newdob,'month':3}
												}
												if(month_name == "May"){
													var day_data = {'name':name,'dob':newdob,'month':4}
												}
												if(month_name == "June"){
													var day_data = {'name':name,'dob':newdob,'month':5}
												}
												if(month_name == "July"){
													var day_data = {'name':name,'dob':newdob,'month':6}
												}
												if(month_name == "August"){
													var day_data = {'name':name,'dob':newdob,'month':7}
												}
												if(month_name == "September"){
													var day_data = {'name':name,'dob':newdob,'month':8}
												}
												if(month_name == "October"){
													var day_data = {'name':name,'dob':newdob,'month':9}
												}
												if(month_name == "November"){
													var day_data = {'name':name,'dob':newdob,'month':10}
												}
												if(month_name == "December"){
													var day_data = {'name':name,'dob':newdob,'month':11}
												}
												$.ajax({                                      
													url: day_api,
													crossDomain: true,
													type: 'GET',
													data: day_data,			
													success: function(data)
													{ 
														var q = data.length;
														for(var p = data.length-1; p >= 0 ; p--){

															var table_day = document.getElementById("table_body_day");
															var row_day = table_day.insertRow(0);
															var cell1_day = row_day.insertCell(0);
															var cell2_day = row_day.insertCell(1);
															var cell3_day = row_day.insertCell(2);
															
															var per_day = ((data[p].day)*10).toFixed(2);

															cell1_day.innerHTML = 'Day '+(q--);
															cell2_day.innerHTML = per_day+'%';
															if(per_day > 0 && per_day <= 48)
															{ 
																cell3_day.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
															}
															else if(per_day > 48 && per_day <= 60)
															{
																cell3_day.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
															}
															else if(per_day > 60 && per_day <= 100)
															{
																cell3_day.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
															}
														}
													}
												});
												$('#detailModal').modal();   
											});
										});
									}
									
								}
								if(category == 3){
									document.getElementById('relationship').style.display = 'block';	
									document.getElementById('category-3').innerHTML = 'Relationship Compatibility';									
									var relation = ["emotional strength ","temperament and tendency","harmony of relationship","tenure of relationship","chances of disagreement","natural adjustment","wealth and property","mutual respect trust","thoughts and feelings","mutual attraction"];
									for(var i=9; i>=0; i--){
										var table = document.getElementById("table-body-3");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);
										cell1.innerHTML = relation[i];
										if(i==9){
											var perc = data[0].J.toFixed(2);
										}
										if(i==8){
											var perc = data[0].I.toFixed(2);
										}
										if(i==7){
											var perc = data[0].H.toFixed(2);
										}
										if(i==6){
											var perc = data[0].G.toFixed(2);
										}
										if(i==5){
											var perc = data[0].F.toFixed(2);
										}
										if(i==4){
											var perc = data[0].E.toFixed(2);
										}
										if(i==3){
											var perc = data[0].D.toFixed(2);
										}
										if(i==2){
											var perc = data[0].C.toFixed(2);
										}
										if(i==1){
											var perc = data[0].B.toFixed(2);
										}
										if(i==0){
											var perc = data[0].A.toFixed(2);
										}
										cell2.innerHTML = perc+'%';
										if(perc > 0 && perc <= 48)
										{ 
											cell3.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell3.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell3.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}										
									}								
								}
								if(category == 4){
									document.getElementById('certainty').style.display = 'block';	
									document.getElementById('category-4').innerHTML = 'Certainty 360';																	
									if(data[0].certainty > 0 && data[0].certainty <= 3)
									{
										document.getElementById('certainty360').innerHTML = '<b>Yes : </b>'+'Yes, Just because of your lucky planetary number  you will get positive results. There is more than 80% chance of the positive result.';				
									}
									if(data[0].certainty > 3 && data[0].certainty <= 6)
									{
										document.getElementById('certainty360').innerHTML = '<b>No : </b>'+'No, this time is not in your favour that\'s why the possibilities of your success is less than 25%.';				
									}
									if(data[0].certainty > 6)
									{
										document.getElementById('certainty360').innerHTML = '<b>May Be : </b>'+'May be you will get positive results or may be not. The probability of your success and getting the right outcome is 50 / 50 percent.';				
									}																
								}
								if(category == 5){
									document.getElementById('kidcareer').style.display = 'block';
									document.getElementById('category-5').innerHTML = 'Career and Profession';									
									var kids = [
												"Politician, Govt.Services, Authority",
												"Doctor, Chef, Import-Export Business, Merchant Navy, Chemical Eng.,Medical Representative",
												"Financer, Pshychologist, Advertising, Editor, Stock Market, Banking",
												"Engineer,  IT, Researcher, Electronic Computer Related  Jobs Business",
												"Teacher, Writer, Self Business, Accountant, Mathematics, Clerk, Merchandising",
												"Art & Music, Entertainment,  Media",
												"Astrology, Numerology, Spiritual, Dream Reader, Plamist, Healer",
												"Mining & Agriculture, Real Estate, Building Trades,Judiciary",
												"Sports & Atheletics, Police, Army, Surgen,Construction Business, Hunter, Fire Fighter"
											  ];
									for(var i=8; i>=0; i--){
										var table = document.getElementById("table-body-4");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);
										cell1.innerHTML = kids[i];

										if(i==8){
											var perc = ((data[0].kid9)*10).toFixed(2);
										}
										if(i==7){
											var perc = ((data[0].kid8)*10).toFixed(2);
										}
										if(i==6){
											var perc = ((data[0].kid7)*10).toFixed(2);
										}
										if(i==5){
											var perc = ((data[0].kid6)*10).toFixed(2);
										}
										if(i==4){
											var perc = ((data[0].kid5)*10).toFixed(2);
										}
										if(i==3){
											var perc = ((data[0].kid4)*10).toFixed(2);
										}
										if(i==2){
											var perc = ((data[0].kid3)*10).toFixed(2);
										}
										if(i==1){
											var perc = ((data[0].kid2)*10).toFixed(2);
										}
										if(i==0){
											var perc = ((data[0].kid1)*10).toFixed(2);
										}

										cell2.innerHTML = perc+'%';

										if(perc > 0 && perc <= 48)
										{ 
											cell3.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width: 42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell3.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width: 42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell3.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width: 42px;margin:auto;'></p>";
										}
									}
								}
								if(category == 6){
									document.getElementById('life').style.display = 'block';
									document.getElementById('category-6').innerHTML = 'Life 360';									
									for(var i=9; i>=0; i--){
										var table = document.getElementById("table-body-5");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);

										if(i==9){
											cell1.innerHTML = 'Spirituality & Travels';											
											var perc = ((data[0].Spiritualityandtravels)*10).toFixed(2);
										}
										if(i==8){
											cell1.innerHTML = 'Respect Gains & Status';													
											var perc = ((data[0].Respectgainsandstatus)*10).toFixed(2);
										}
										if(i==7){
											cell1.innerHTML = 'Property & Vehicle';														
											var perc = ((data[0].Propertyandvehicle)*10).toFixed(2);
										}
										if(i==6){
											cell1.innerHTML = 'Power & Higher Authority';												
											var perc = ((data[0].PowerandhigherAuthority)*10).toFixed(2);
										}
										if(i==5){
											cell1.innerHTML = 'Merried Life';												
											var perc = ((data[0].Merriedlife)*10).toFixed(2);
										}
										if(i==4){
											cell1.innerHTML = 'Health';														
											var perc = ((data[0].Health)*10).toFixed(2);
										}
										if(i==3){
											cell1.innerHTML = 'Finance & Profits';														
											var perc = ((data[0].FinanceandProfits)*10).toFixed(2);
										}
										if(i==2){
											cell1.innerHTML = 'Family';														
											var perc = ((data[0].Family)*10).toFixed(2);
										}
										if(i==1){
											cell1.innerHTML = 'Emotional Stability';														
											var perc = ((data[0].EmotionalStability)*10).toFixed(2);
										}
										if(i==0){
											cell1.innerHTML = 'Career & Education';												
											var perc = ((data[0].CareerandEducation)*10).toFixed(2);
										}

										cell2.innerHTML = perc+'%';

										if(perc > 0 && perc <= 48)
										{ 
											cell3.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell3.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell3.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
									}									
								}
								if(category == 7){
									document.getElementById('vivah').style.display = 'block';
									document.getElementById('category-7').innerHTML = 'Vivah Sutra';									
									var vivah = ["Intensity of relationship ","Temperament and behavior","Potential possibility","Mutual compatibility and attraction","Matrimonial harmony","Tenure of relationship","Chances of disagreement","Natural adjustment","Affluence and property","Thoughts and feelings"];			
									for(var i=9; i>=0; i--){
										var table = document.getElementById("table-body-6");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);
										cell1.innerHTML = vivah[i];
										if(i==9){
											var perc = (data[0].J).toFixed(2);
										}
										if(i==8){
											var perc = (data[0].I).toFixed(2);
										}
										if(i==7){
											var perc = (data[0].H).toFixed(2);
										}
										if(i==6){
											var perc = (data[0].G).toFixed(2);
										}
										if(i==5){
											var perc = (data[0].F).toFixed(2);
										}
										if(i==4){
											var perc = (data[0].E).toFixed(2);
										}
										if(i==3){
											var perc = (data[0].D).toFixed(2);
										}
										if(i==2){
											var perc = (data[0].C).toFixed(2);
										}
										if(i==1){
											var perc = (data[0].B).toFixed(2);
										}
										if(i==0){
											var perc = (data[0].A).toFixed(2);
										}
										cell2.innerHTML = perc+'%';
										if(perc > 0 && perc <= 48)
										{ 
											cell3.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell3.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell3.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}										
									}								
								}
								if(category == 9){
									document.getElementById('pastlife').style.display = 'block';
									document.getElementById('category-9').innerHTML = 'Past Life';																		
									document.getElementById('past-life1').innerHTML = '<b>Description : </b>'+data.description;
									document.getElementById('past-life2').innerHTML = '<b>Occupation : </b>'+data.occupation;																
									document.getElementById('past-life3').innerHTML = '<b>Quote : </b>'+data.quote;																																									
								}
								if(category == 10){
									document.getElementById('pain_gain').style.display = 'block';
									document.getElementById('category-10').innerHTML = 'Annual Pain & Gain';									
									for(var i=9; i>=0; i--){
										var table = document.getElementById("table-body-7");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);

										if(i==9){
											cell1.innerHTML = 'Spirituality & Travels';											
											var perc = ((data[0].Spiritualityandtravels)*10).toFixed(2);
										}
										if(i==8){
											cell1.innerHTML = 'Respect Gains & Status';													
											var perc = ((data[0].Respectgainsandstatus)*10).toFixed(2);
										}
										if(i==7){
											cell1.innerHTML = 'Property & Vehicle';														
											var perc = ((data[0].Propertyandvehicle)*10).toFixed(2);
										}
										if(i==6){
											cell1.innerHTML = 'Power & Higher Authority';												
											var perc = ((data[0].PowerandhigherAuthority)*10).toFixed(2);
										}
										if(i==5){
											cell1.innerHTML = 'Merried Life';												
											var perc = ((data[0].Merriedlife)*10).toFixed(2);
										}
										if(i==4){
											cell1.innerHTML = 'Health';														
											var perc = ((data[0].Health)*10).toFixed(2);
										}
										if(i==3){
											cell1.innerHTML = 'Finance & Profits';														
											var perc = ((data[0].FinanceandProfits)*10).toFixed(2);
										}
										if(i==2){
											cell1.innerHTML = 'Family';														
											var perc = ((data[0].Family)*10).toFixed(2);
										}
										if(i==1){
											cell1.innerHTML = 'Emotional Stability';														
											var perc = ((data[0].EmotionalStability)*10).toFixed(2);
										}
										if(i==0){
											cell1.innerHTML = 'Career & Education';												
											var perc = ((data[0].CareerandEducation)*10).toFixed(2);
										}

										cell2.innerHTML = perc+'%';

										if(perc > 0 && perc <= 48)
										{ 
											cell3.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell3.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell3.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
									}											
								}
								if(category == 11){
									document.getElementById('my_favourite').style.display = 'block';
									document.getElementById('category-11').innerHTML = 'My Favourite';									
									document.getElementById('my_favourite1').innerHTML = '<b>Favourite Days = </b>'+data[0].Days;
									document.getElementById('my_favourite2').innerHTML = '<b>Favourite Color = </b>'+data[0].Favourite_color;
									document.getElementById('my_favourite3').innerHTML = '<b>Favourite Planets = </b>'+data[0].Planets;
									document.getElementById('my_favourite4').innerHTML = '<b>Favourite Cities = </b>'+data[0].citiescontries;
									document.getElementById('my_favourite5').innerHTML = '<b>Favourite Diseases = </b>'+data[0].diseases;
									document.getElementById('my_favourite6').innerHTML = '<b>Favourite Food = </b>'+data[0].food_dish;
									document.getElementById('my_favourite7').innerHTML = '<b>Favourite Medical = </b>'+data[0].medicalprecautions;
									document.getElementById('my_favourite8').innerHTML = '<b>Favourite Months = </b>'+data[0].months;
									document.getElementById('my_favourite9').innerHTML = '<b>Favourite Letters = </b>'+data[0].suitable_letters;
									document.getElementById('my_favourite10').innerHTML = '<b>Favourite God = </b>'+data[0].suitablegod;
									document.getElementById('my_favourite11').innerHTML = '<b>Favourite Number = </b>'+data[0].id;
									
								}
								if(category == 12){
									document.getElementById('category12').style.display = 'block';
									document.getElementById('category-12').innerHTML = 'Hourly Prediction';																		
									var j=24;
									for(var i=23; i>=0; i--){
										var table = document.getElementById("table-body-2");
										var row = table.insertRow(0);
										var cell1 = row.insertCell(0);
										var cell2 = row.insertCell(1);
										var cell3 = row.insertCell(2);
										var cell4 = row.insertCell(3);
										cell1.innerHTML = (j--)+'.';
										
										cell2.innerHTML = data[i].time;
										
										var perc = ((data[i].hora)*10).toFixed(2);

										cell3.innerHTML = perc+'%';

										if(perc > 0 && perc <= 48)
										{ 
											cell4.innerHTML = "<p style='background:red; color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 48 && perc <= 60)
										{
											cell4.innerHTML = "<p style='background:yellow;color:#000;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
										else if(perc > 60 && perc <= 100)
										{
											cell4.innerHTML = "<p style='background:green;color:#f2f2f2;border-radius:50%;height:42px;width:42px;margin:auto;'></p>";
										}
									}												
								}
							},
							error: function(error){
								console.log(error);
								document.getElementById('form-two-style').style.display = 'none';								   
								document.getElementById('form-one-style').style.display = 'block';
							} 
						});	
					}
					else{           
						document.getElementById('mandatory_field').style.display = 'block';	
						setInterval(mandatory,6000);
						function mandatory(){
							document.getElementById('mandatory_field').style.display = 'none';				
						}
					}
				}
			</script>

			<div class="row home-contents">
				<div class="row">	
					<!-- HEADING AND BUTTONS -->
					<div class="intro-section">
						<h3 class="intro text-center">Astrosum, An Innovation of Gravity E-com, helps you to find out “Best Horoscope” of Your Life.</h3>
					</div>	
				</div>	
			</div>
			<!-- /END ROW -->	
		</div>
			<!-- /END CONTAINER -->
	</div>
	<!-- /END COLOR OVERLAY -->
</header>
<!-- /END HEADER -->

<!-- =========================
     FEATURES 
============================== -->
<section class="app-brief" id="features" style="padding-top: 20px; padding-bottom: 20px;">

<div class="container">
	<!-- SECTION TITLE -->
	
	<h3 class="dark-text text-center">Why We Best</h3>	
	
	<div class="row">
		
		<!-- BRIEF -->
	
		<div class="col-md-8 left-align wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
			
			
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">Easy launch widget.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">Daily Notification.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">100% Reliable and Trusted.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">Personalized Weekly Horoscope.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">User attractive and User Interactive Design.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">Help to know about Auspicious and Inauspicious hours of each and every day.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">100% Protected and User friendly.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">You can keep astrosum open when a phone call or a text message interrupts your reading.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4" id="card-background">
				<div class="card" style="padding:10px;">
					<div class="card-block">
						<p class="card-text" style="margin-top: 0px;">Minimum loading time.</p>
					</div>
				</div>
			</div>           
		</div>

		<!-- /ENDBRIEF -->
		
		<!-- PHONES IMAGE -->
		<div class="col-md-4 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
			<div class="phone-image">
				<img src="images/pic1.png" alt="">
			</div>
		</div>
		<!-- /END PHONES IMAGE -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
	
<!-- /END FEATURES SECTION -->

<section class="app-brief grey-bg" id="services">
	<!--Category Start  -->
	<div class="category">
		<div class="section-header wow fadeIn animated animated" data-wow-offset="120" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeIn;">
			<!-- SECTION TITLE -->
			<h2 class="dark-text">Our Amazing  Life Time Services</h2>
			<div class="colored-line"></div>
			<div class="section-description">
			 Astrosum is a most valuable and scientific application which boost your life efficiency in every possible manner. Astrosum not only studies your birth-Chart but also predict about your life. Our study has based on Vedic Astrology with full of accuracy and brief review by our experts.
			</div>
			<div class="colored-line"></div>
		</div>

		<!--Carousel Image Start  -->
		<div class="carousel-image" id="carousel-image">
			<div class="row carousel-row-style">
				<?php
					include("database/database.php");
					$counter=0;					
					$select_data="select * from category where status = 1";
					$run_data=mysqli_query($conn, $select_data);
					while($row=mysqli_fetch_array($run_data)):
					
					$id = $row['id'];
					$category_name=$row['name'];
					$category_description=$row['description'];
					$category_image=$row['category_pic'];
				?>
				<div class="col-md-3 card-col-style-1">
					<div class="card header-card-style-1">
						<img class="card-image-style1" src="<?php echo $category_image; ?>">
						<div class="card-block text-center header-card-block-style-1">
							<h5 class="card-title" style="font-family:'Roboto', sans-serif;font-weight:bold;"><?php echo $category_name; ?></h5>
							<p class="card-text" style="font-family:'Roboto', sans-serif;">
								<?php 
									$newStr = preg_replace("/[^a-zA-Z0-9,.\s]/", "", $category_description);
									$cat_str = substr($newStr,0, 95);
									echo $cat_str; 
								?>
							</p>
							<a class="dial" id="<?=$id;?>" style="cursor:pointer;">Read More</a>
						</div>
						<button onclick="changeButton(<?=$id;?>)" style="width: 100%; text-align: center; margin: auto; border-radius:0px; background:#FFA500 !important;border: none;color: aliceblue;padding:5px;">See Future</button>
					</div>
				</div>				
				<?php endwhile; ?>
			</div> 
		</div>
		      
		<!--Carousel Image End  -->
		<script>
		function changeButton(id){
			$.ajax({                                      
				url: 'php-file/get-category-modal.php',
				type: 'GET',
				data: {number:id},   
				success: function(data)
				{   
					var indexValue = 0;
					
					var innerValue = data;

					if(id == 1){
						indexValue = 1;
					}
					else if(id == 2){
						indexValue = 2;
					}
					else if(id == 3){
						indexValue = 3;
						document.getElementById('pname1').style.display = 'block';
						document.getElementById('dob1').style.display = 'block';
						document.getElementById('pname2').style.display = 'block';
						document.getElementById('dob2').style.display = 'block';
                        document.getElementById('your_name_category').style.display = 'none';

					}
					else if(id == 4){
						indexValue = 4;
						document.getElementById('question').style.display = 'block';
                        var type_value = document.getElementById('get_user_question').value;	
						if(type_value == ''){
							document.getElementById('category_question').style.display = 'block';
						}else{
							document.getElementById('category_question').style.display = 'none';
						}
					}
					else if(id == 5){
						indexValue = 5;
					}
					else if(id == 6){
						indexValue = 6;
					}
					else if(id == 7){
						indexValue = 7;
						document.getElementById('mname').style.display = 'block';
						document.getElementById('mdob').style.display = 'block';
						document.getElementById('fname').style.display = 'block';
						document.getElementById('fdob').style.display = 'block';
                        document.getElementById('your_name_category').style.display = 'none';

					}
					else if(id == 8){
						indexValue = 8;
						document.getElementById('type').style.display = 'block';
                        var type_value = document.getElementById('get_user_type').value;	
						if(type_value == ''){
							document.getElementById('category_type_display').style.display = 'block';
						}						
					}
					else if(id == 9){
						indexValue = 9;
					}
					else if(id == 11){
						indexValue = 10;
					}
					else if(id == 12){
						indexValue = 11;
					}

					$("#modal-title").html(data);
					
					$('#get_user_category option')[indexValue].selected = true;
					
					$('#getpredictionModal').modal('toggle');   
					
				} 
			});
		}
		</script>
		<!--Read More Modal Start-->
		<div class="modal fade" id="readmoreModal" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header" style="background:#FFA500 !important; color:aliceblue;">
						<h4 class="modal-title" id="read-title"></h4>						
						<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<p align="justify" style="font-family: 'Roboto', sans-serif;" id="read-desc"></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-success" style="background:#FFA500 !important; color:aliceblue;" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> 
		<!--Read More Modal End  -->
	</div>
	<!--Category End  -->   
</section>
<!-- /END SECTION -->

<div class="section-header wow fadeIn animated animated" data-wow-offset="120" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeIn;">
		
		<!-- SECTION TITLE -->
		<h2 class="dark-text">Amazing Core Features</h2>
		<div class="colored-line">
		</div>
		<div class="section-description">
			Plan your future with Astrosum- A revolutionary platform of Gravity.
		</div>
		<div class="colored-line">
		</div>
	</div>
    <div class="row">
		
		<!-- FEATURES LEFT -->
		<div class="col-md-4 col-sm-4 features-left wow fadeInLeft animated animated" data-wow-offset="10" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInLeft;">
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_map_alt"></i>
					</div>
                    </div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Auspicious Hours</h4>
					<p>
					All about best timing. So boost up your success without any hurdle and get a fruitful result. Doing work in auspicious hour help to save your present and secure your future. 
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_tablet"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Inauspicious Hours</h4>
					<p>
					Working hard and getting zero could result in damaging your workability, increasing hurdles, raising irritation and may disqualify you. Astrosum helps you to secure your future by knowing about each of the inauspicious hours in advance.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_gift_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Daily/Weekly Prediction</h4>
					<p>
					Daily or weekly prediction is a fantastic feature of Astrosum, where you can know about your every type of prediction on the daily or weekly basis.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- google ad -->
			
			<!-- /END google ad -->
			
		</div>
		<!-- /END FEATURES LEFT -->
		
		<!-- PHONE IMAGE -->
		<div class="col-md-4 col-sm-4">
			<div class="phone-image wow bounceIn animated animated" data-wow-offset="120" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: bounceIn;">
				<img src="images/pic1.png" alt="" width="260px">
			</div>
		</div>
		
		<!-- FEATURES RIGHT -->
		<div class="col-md-4 col-sm-4 features-right wow fadeInRight animated animated" data-wow-offset="10" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInRight;">
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_genius"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Health Prediction</h4>
					<p>
					Feel refresh and be always a health freak by knowing the correct movement of your stars. So you’ll be able to know about your future health in present time with more accuracy.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_lightbulb_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Work Predictions</h4>
					<p>
					Wants to achieve a new height of success but failed every time. Then this is the time when Astrosum can help you by providing an hourly prediction, Weekly prediction, monthly prediction, yearly prediction and much more.
					</p>
				</div>
				
			</div>
			
			<!-- /END SINGLE FEATURE -->
			
			<!-- FEATURE -->
			<div class="feature">
				
				<!-- ICON -->
				<div class="icon-container">
					<div class="icon">
						<i class="icon_ribbon_alt"></i>
					</div>
				</div>
				
				<!-- FEATURE HEADING AND DESCRIPTION -->
				<div class="fetaure-details">
					<h4 class="main-color">Love/Relationship Predictions</h4>
					<p>
					Love or Relationship is a most important thing for everyone. So find your best and lucky day for love and romance with full on accuracy. Astrosum makes typical calculations for you to provide the best prediction with maintaining accuracy.
					</p>
				</div>
				
			</div>
			<!-- /END SINGLE FEATURE -->
		</div>
        <div class="clearfix"></div><br>
		<!-- /END FEATURES RIGHT -->
		
	</div>
    
    <section class="app-brief grey-bg" id="horoscope">

<div class="annual-report">
<div class="section-header wow fadeIn animated animated" data-wow-offset="120" data-wow-duration="1.5s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeIn;">
		
		<!-- SECTION TITLE -->
		<h2 class="dark-text">Total Horoscope</h2>
		<div class="colored-line">
		</div>
		<div class="section-description">
			Astrosum- Helps you to get the detailed horoscope for each zodiac sign separately in a personalised way. Click on your zodiac sign and get your personalised horoscope services.
		</div>
		<div class="colored-line">
		</div>
    </div>
    
<!--Zodiac Sign Start  -->
<div class="carousel-image" id="carousel-image" style="background-color:cadetblue !important;">
  <div class="row carousel-row-style">
    <?php
        include("database/database.php");
        $select_data="select * from astrology";
        $run_data=mysqli_query($conn, $select_data);
        while($row=mysqli_fetch_array($run_data))
        {
        $id = $row['id'];
        $tagline=$row['tagline'];
        $description=$row['description'];
        $image=$row['image'];
    ?>
    <div class="col-md-3 animated zoomIn card-col-style">
        <div class="card header-card-style">
            <img class="card-image" src="assets/images/<?php echo $image; ?>">
            <div class="card-block text-center header-card-block-style">
                <h5 class="card-title"><?php echo $tagline; ?></h5>
                <p class="card-text"><?php echo $description; ?></p>
            </div>
            <button type="button" class="btn btn-success openHoroscopeModal" id="<?=$id;?>" style="width: 110px; text-align: center; margin: auto;background:#FFA500 !important;">Get Details</button>
        </div>
    </div>
    <?php }; ?>
  </div> 
</div>
<style>
	.modal-dialog{
		overflow-y: initial !important
	}
	#zodiac-body{
		height: 500px;
		overflow-y: auto;
	}
</style>
<div class="modal fade" id="horoscopeModal" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog" role="document">
  <div class="modal-content">
	<div class="modal-header" style="background:#FFA500 !important; color:aliceblue;">
	  <h5 class="modal-title" id="exampleModalLabel"><strong id="horoscope-title"></strong></h5>
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
		<span aria-hidden="true">&times;</span>
	  </button>
	</div>
	<div class="modal-body" id="zodiac-body">
	<h4 id="zodiac_mandatory_field" style="display:none; text-align:center;">All fields are Mandatory.</h4>	
	  <form id="horoscope-1">
		<div class="form-group">
		  <label for="zodiac" class="form-control-label" style="float:left; color: coral;">Zodiac/राशि</label>
		  <input type="text" class="form-control" id="horoscope_user_zodiac" readonly>
		</div>
		<div class="form-group">
		  <label for="name" class="form-control-label" style="float:left; color: coral;">Name/नाम</label>
		  <input type="text" class="form-control" onkeyup="validateName()" id="horoscope_user_name">
		  <small id="horoscope_name" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Only charecter are allowed.</small>
		</div>
		<div class="form-group">
		  <label for="email" class="form-control-label" style="float:left; color: coral;">Email/ईमेल</label>
		  <input type="text" class="form-control" onkeyup="validateEmail()" id="horoscope_user_email">
		  <small id="horoscope_email" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Invalid Email.</small>
		</div>
		<div class="form-group">
		  <label for="dob" class="form-control-label" style="float:left; color: coral;">Date of Birth/जन्मदिन</label>
		  <input type="date" class="form-control" onkeyup="validateDOB()" id="horoscope_user_dob">
		  <small id="horoscope_dob" style="width:100%;text-align:left;color:red;display:none;float:left;font-weight:600;font-size:small;">Please enter a valid date of birth.</small>
		</div>
		<div class="form-group">
		  <label for="birthtime" class="form-control-label" style="float:left; color: coral;">Birth Time/जन्म का समय</label>
		  <input type="text" class="form-control" id="horoscope_user_time">
		</div>
		<div class="form-group">
		  <label for="birthplace" class="form-control-label" style="float:left; color: coral;">Birth Place/जन्म स्थान</label>
		  <input type="text" class="form-control" id="horoscope_user_place">
		</div>
	  </form>
	  <form id="horoscope-2" style="display:none">
		  <h4 class="text-center">Your Zodiac Result</h4>
		  <div class="row">
			  <div class="col-md-12">
				  <p id="prediction_date" style="text-align:left;"></p>
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_sun_sign" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_emotion" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_health" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_luck" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_life" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_profession" style="text-align:left;"></p>					
			  </div>
			  <div class="col-md-12">
				  <p id="prediction_travel" style="text-align:left;"></p>					
			  </div>
		  </div>
	  </form>
	</div>
	<div class="modal-footer" style="margin-top:0px;">
	  <button type="button" id="horoscope_close_button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
	  <button id="horoscope_user_button" onclick="horoscopeUserDatabase();" class="btn btn-success" id="button" style="background:#FFA500 !important; color:aliceblue;">Submit</button>
	</div>
  </div>
</div>
</div>
<!--Zodiac End  -->
<script type="text/javascript">
function horoscopeUserDatabase(){

	var name = document.getElementById('horoscope_user_name').value;
	var email = document.getElementById('horoscope_user_email').value;
	var dob = document.getElementById('horoscope_user_dob').value;
	var time = document.getElementById('horoscope_user_time').value;
	var place = document.getElementById('horoscope_user_place').value;
	var zodiac_sign = document.getElementById('horoscope_user_zodiac').value;		
	if(email != '' && name != '' && dob != '' && time != '' && place != '' && zodiac_sign != '')
	{
		$.ajax({                                      
			url: 'http://astrosum.com/astrosum_api/astroapi/src/prediction.php',
			crossDomain:true,
			type: 'GET',
			data: {'zodiac':zodiac_sign},   
			success: function(data)
			{   
				var resData = JSON.parse(data);
				document.getElementById('horoscope-1').style.display = 'none';										
				document.getElementById('horoscope-2').style.display = 'block';	
				
				document.getElementById('prediction_date').innerHTML = "<b>Date :</b> "+ resData[0].prediction_date;
				document.getElementById('prediction_sun_sign').innerHTML = "<b>Sun Sign :</b> "+ resData[0].sun_sign;									
				document.getElementById('prediction_emotion').innerHTML = "<b>Emotion :</b> "+ resData[0].prediction.emotions;
				document.getElementById('prediction_health').innerHTML = "<b>Health :</b> "+ resData[0].prediction.health;
				document.getElementById('prediction_luck').innerHTML = "<b>Luck :</b> "+ resData[0].prediction.luck;
				document.getElementById('prediction_life').innerHTML = "<b>Personal Life :</b> "+ resData[0].prediction.personal_life;
				document.getElementById('prediction_profession').innerHTML = "<b>Profession :</b> "+ resData[0].prediction.profession;
				document.getElementById('prediction_travel').innerHTML = "<b>Travel :</b> "+ resData[0].prediction.travel;									
				
				document.getElementById('horoscope_close_button').style.display = 'none';
				document.getElementById('horoscope_user_button').style.display = 'none';		  
			} 
		});	
	}
	else{
		document.getElementById('zodiac_mandatory_field').style.display = 'block';	
		setInterval(mandatory,6000);
		function mandatory(){
			document.getElementById('zodiac_mandatory_field').style.display = 'none';				
		}	  		
	}
}
</script>
<!-- Modal -->
</div>
</section>
<!-- =========================
     TESTIMONIALS 
============================== -->
<section class="testimonials" id="testimonial">

<div class="color-overlay">
	
	<div class="container wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- FEEDBACKS -->
		<div id="feedbacks" class="owl-carousel owl-theme">
			
			<!-- SINGLE FEEDBACK -->
			<div class="feedback">
				
				<!-- IMAGE -->
				<div class="image">
					<!-- i class=" icon_quotations"></i -->
					<img src="images/rajeev.jpg" alt="">
				</div>
				
				<div class="message">
					Let me warn you only one out of about 1000 astrologers genuine knows astrology rest are out to make money by fooling general public. Astrosum Team real work hard to give accurate result to make ease in your life.
				</div>
				
				<div class="white-line">
				</div>
				
				<!-- INFORMATION -->
				<div class="name">
					Rajeev Sharma
				</div>
				<div class="company-info">
					CEO, Gravity Ecom Business Pvt Ltd.
				</div>
				
			</div>
			<!-- /END SINGLE FEEDBACK -->
			
			<!-- SINGLE FEEDBACK -->
			
			<!-- /END SINGLE FEEDBACK -->
			
			<!-- SINGLE FEEDBACK -->
		
			<!-- /END SINGLE FEEDBACK -->
			
		</div>
		<!-- /END FEEDBACKS -->
		
	</div>
	<!-- /END CONTAINER -->
	
</div>
<!-- /END COLOR OVERLAY -->

</section>
<!-- /END TESTIMONIALS SECTION -->

<!-- =========================
     SCREENSHOTS
============================== -->
<section class="screenshots grey-bg" id="screenshot-section">

<div class="container">
	
	<!-- SECTION HEADER -->
	<div class="section-header wow fadeIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- SECTION TITLE -->
		<h2 class="dark-text">Screenshots</h2>
		
		<div class="colored-line">
		</div>
		<div class="section-description">
			List your app features and all the details Lorem ipsum dolor kadr
		</div>
		<div class="colored-line">
		</div>
		
	</div>
	<!-- /END SECTION HEADER -->
	
	<div class="row wow bounceIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<div id="screenshots" class="owl-carousel owl-theme">
	
			<div class="shot">
				<a href="images/screenshots/2.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/2.png" alt="Screenshot"></a>
			</div>
			
			<div class="shot">
				<a href="images/screenshots/3.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/3.png" alt="Screenshot"></a>
			</div>
			
			<div class="shot">
				<a href="images/screenshots/4.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/4.png" alt="Screenshot"></a>
			</div>
			
			<div class="shot">
				<a href="images/screenshots/5.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/5.png" alt="Screenshot"></a>
			</div>
			
			<div class="shot">
				<a href="images/screenshots/6.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/6.png" alt="Screenshot"></a>
			</div>
			
			<div class="shot">
				<a href="images/screenshots/7.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/7.png" alt="Screenshot"></a>
			</div>
			<div class="shot">
				<a href="images/screenshots/8.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/8.png" alt="Screenshot"></a>
			</div>
			<div class="shot">
				<a href="images/screenshots/9.png" data-lightbox-gallery="screenshots-gallery"><img src="images/screenshots/9.png" alt="Screenshot"></a>
			</div>
		</div>
		<!-- /END SCREENSHOTS -->
		
	</div>
	<!-- /END ROW -->
	
</div>
<!-- /END CONTAINER -->

</section>
<!-- /END SCREENSHOTS SECTION -->

<!-- =========================
     DOWNLOAD NOW 
============================== -->
<section class="download" id="download">

<div class="color-overlay">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<!-- DOWNLOAD BUTTONS AREA -->
				<div class="download-container">
					<h2 class=" wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">Download the app on</h2>
					
					<!-- BUTTONS -->
					<div class="buttons wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
						
                        <a href="https://play.google.com/store/apps/details?id=com.GravityDailyHorscope" class="btn btn-default btn-lg standard-button"><i class="icon-google-play"></i>Play Store</a>
						<a href="#" class="btn btn-default btn-lg standard-button"><i class="icon-app-store"></i>Apple Store</a>
						
					</div>
					<!-- /END BUTTONS -->
					
				</div>
				<!-- END OF DOWNLOAD BUTTONS AREA -->
				
				<!-- SUBSCRIPTION FORM WITH TITLE -->
				<div class="subscription-form-container">
					
					<h2 class="wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">Subscribe Now!</h2>
					
					<!-- =====================
					     MAILCHIMP FORM STARTS 
					     ===================== -->
					
					<form class="subscription-form form-inline wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s" role="form">
						
						<!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
						<h4 class="subscription_success" style="display:none;"><i class="icon_check"></i> Thank you! We will notify you soon.</h4>
						<h4 class="subscription_error" style="display:none;"><i class="icon_close"></i>Something Wrong!</h4>
						<h4 class="invalid_email" style="display:none;"><i class="icon_close"></i>Invalid email, Please try again!</h4>						
						
						<!-- EMAIL INPUT BOX -->
						<input type="email" name="email" id="subscriber_email" placeholder="Your Email" class="form-control input-box">
						
						<!-- SUBSCRIBE BUTTON -->						
					</form>
					<button onclick="subscribeme()" class="btn btn-default standard-button">Subscribe</button>
					
					<!-- /END MAILCHIMP FORM STARTS -->
					
					<!-- =====================
					     LOCAL TXT FORM STARTS 
					     ===================== -->
				</div>
                <!-- END OF SUBSCRIPTION FORM WITH TITLE -->
				
			</div> 
			<!-- END COLUMN -->
			
		</div> 
		<!-- END ROW -->
		
	</div>
	<!-- /END CONTAINER -->
</div>
<!-- /END COLOR OVERLAY -->
</section>
<!-- /END DOWNLOAD SECTION -->
<!-- =========================
     FOOTER 
============================== -->
<footer id="contactus">

<div class="container">
	
	<div class="contact-box wow rotateIn animated" data-wow-offset="10" data-wow-duration="1.5s">
		
		<!-- CONTACT BUTTON TO EXPAND OR COLLAPSE FORM -->
		
		<a class="btn contact-button expand-form expanded"><i class="icon_mail_alt"></i></a>
		
		<!-- EXPANDED CONTACT FORM -->
		<div class="row expanded-contact-form">
			
			<div class="col-md-8 col-md-offset-2">
				
				<!-- FORM -->
				<form class="contact-form" method="post">
					
					<!-- IF MAIL SENT SUCCESSFULLY -->
					<h4 class="success">
						<i class="icon_check"></i> Your message has been sent successfully.
					</h4>
					
					<!-- IF MAIL SENDING UNSUCCESSFULL -->
					<h4 class="error">
						<i class="icon_error-circle_alt"></i>Something went wrong, Please try again!.
					</h4>
					
                     <h4 class="invalid">
						<i class="icon_error-circle_alt"></i>Invalid Form!.
					</h4>

					<div class="col-md-6">
						<input class="form-control input-box" id="send-name" type="text" name="name" placeholder="Your Name">
					</div>
					
					<div class="col-md-6">
						<input class="form-control input-box" id="send-email" type="email" name="email" placeholder="Your Email">
					</div>
					
					<div class="col-md-12">
						<input class="form-control input-box" id="send-subject" type="text" name="subject" placeholder="Subject">
						<textarea class="form-control textarea-box" id="send-message" rows="8" placeholder="Message"></textarea>
					</div>					
				</form>
				<button class="btn btn-primary standard-button2 ladda-button" onClick="sendContact();" data-style="expand-left">Send Message</button>				
				<!-- /END FORM -->
			</div>	
		</div>
		<!-- /END EXPANDED CONTACT FORM -->
	</div>
	<!-- /END CONTACT BOX -->
	<!-- LOGO -->
	<img src="images/logo.png" width="200" height="200" alt="LOGO" class="responsive-img">
	<!-- SOCIAL ICONS -->
    <ul class="social-icons">
		<li><a href="https://www.facebook.com/astrosum/" target="_blank"><i class="social_facebook_square"></i></a></li>
		<li><a href="https://twitter.com/astro_sum" target="_blank"><i class="social_twitter_square"></i></a></li>
		<li><a href="https://www.instagram.com/_astrosum/" target="_blank"><i class="social_instagram_square"></i></a></li>
		<li><a href="https://plus.google.com/u/0/109615864145699282786" target="_blank"><i class="social_googleplus_square"></i></a></li>
	</ul>	
	<!-- COPYRIGHT TEXT -->
	<font size="2px"><table width="100%"><tbody><tr><td align="left"> <a href="aboutus.php" style="color: #737373">About Us</a> | <a href="TermsService.php" style="color:#737373"> Terms of Service
  </a> | <a href="PrivacyPolicy.php" style="color: #737373">Privacy Policy</a> | <a href="Disclaimer.php" style="color: #737373">Disclaimer</a> | <a href="faqs.php" style="color: #737373">FAQs</a></td>
	<!-- COPYRIGHT TEXT -->
	<td align="left">
		©2017 Gravity, All Rights Reserved | Powered by <a href="http://www.gravityecom.com/">Gravity Ecom Business Pvt. Ltd.</a>
	</td>
     <td> </td>
    </tr></tbody></table></font>
</div>
<!-- /END CONTAINER -->	
</footer>
<!-- /END FOOTER -->
<!-- =========================
     SCRIPTS 
============================== -->
<script>
	$(document).ready(function() {
		
		$(".modal").on("hidden.bs.modal", function() {
			
			$('#loginsignupModal').modal('hide');									
			$('#getpredictionModal').modal('hide');						
			$('#detailModal').modal('hide');			
			$('td').css("display", "none");											

			document.getElementById('form-one-style').style.display = 'none';
			document.getElementById('your_name_category').style.display = 'block';
			
			document.getElementById('my_favourite').style.display = 'none';
			document.getElementById('pain_gain').style.display = 'none';
			document.getElementById('vivah').style.display = 'none';
			document.getElementById('life').style.display = 'none';
			document.getElementById('kidcareer').style.display = 'none';
			document.getElementById('pastlife').style.display = 'none';
			document.getElementById('certainty').style.display = 'none';
			document.getElementById('relationship').style.display = 'none';	
			document.getElementById('category12').style.display = 'none';
			document.getElementById('category128').style.display = 'none';

			document.getElementById('category-tag').innerHTML = "";
			document.getElementById('category-3').innerHTML = "";
			document.getElementById('category-4').innerHTML = "";
			document.getElementById('category-5').innerHTML = "";
			document.getElementById('category-6').innerHTML = "";
			document.getElementById('category-7').innerHTML = "";
			document.getElementById('category-9').innerHTML = "";
			document.getElementById('category-10').innerHTML = "";
			document.getElementById('category-11').innerHTML = "";
			document.getElementById('category-12').innerHTML = "";											
			
			document.getElementById('form-two-style').style.display = 'block';
			document.getElementById('type').style.display = 'none';
			document.getElementById('pname1').style.display = 'none';
			document.getElementById('dob1').style.display = 'none';
			document.getElementById('pname2').style.display = 'none';
			document.getElementById('dob2').style.display = 'none';
			document.getElementById('question').style.display = 'none';	
			document.getElementById('mname').style.display = 'none';
			document.getElementById('mdob').style.display = 'none';
			document.getElementById('fname').style.display = 'none';
			document.getElementById('fdob').style.display = 'none';	

			document.getElementById('modal-result').style.display = 'none';																								
			document.getElementById('modal-title').style.display = 'block';																
			document.getElementById('modal-footer').style.display = 'block';	

			document.getElementById('horoscope-1').style.display = 'block';										
			document.getElementById('horoscope-2').style.display = 'none';	
							
			document.getElementById('horoscope_close_button').style.display = 'inline-block';
			document.getElementById('horoscope_user_button').style.display = 'inline-block';   
																										
		});
	});
</script>

<script src="js/bootstrap.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/retina.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/matchMedia.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/custom.js"></script>
<script src="js/main.js"></script>
<script src="js/rotate.js"></script>

</body>

</html>